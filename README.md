# Android-Open-Map

Android project that displays map tiles but also tracking and sending on a Garmin watch.

## Display a map
You can add directly your url in the application but it also manages the mbtiles

Put your mbtiles file in the following directory ``Android/data/com.ravenfeld.maps/files/``

To create your mbtiles I recommend https://mobac.sourceforge.io/

You can visit this site if you do not know a TMS service.
https://wiki.openstreetmap.org/wiki/Tile_servers

## Add key Graphhopper
You have the position to use Graphhopper routing to help you plot. To do this enter your key in the parameters.

Make an account here. https://www.graphhopper.com/

## Add routing engine offline
I tried to take into account the different routers I know but it was often in C or very resource-intensive.

I found a project https://github.com/jgrunert/Osm-Routing-App which allowed me to create my own simple routing system but which consumes almost nothing.

Go to the folder ``OsmConversionPipeline`` to use the program to convert a PBF into a file for the Android application.

Put the file ``osm_routing.zip`` in ``Android/data/com.ravenfeld.maps/files/``

At restart the application will unzip the file, it may take a while before the service is operational. 

## Add elevation data

To add the elevation management you have to put your HGT files in ``Android/data/com.ravenfeld.maps/files/dem``

https://search.earthdata.nasa.gov/search

or for France

https://data.europa.eu/data/datasets/dtm-france?locale=fr

## Send my track on Garmin

To send your track to your garmin, you need to download the Connect IQ ImportMapsViewer application.

The source code is available here.

https://gitlab.com/ravenfeld/importmapsviewer

## Source having helped to code this project.
https://github.com/jgrunert/Osm-Routing-App

https://github.com/mapsforge/mapsforge

https://github.com/osmdroid/osmdroid

https://github.com/maplibre/maplibre-gl-native
