package com.ravenfeld.maps.data.utils

import com.ravenfeld.maps.domain.geo.GeoPoint
import org.junit.Assert
import org.junit.Test

class RamerDouglasPeuckerTest {

    @Test
    fun testEpsilon1() {
        val pointList = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(1.0, 0.1),
            GeoPoint(2.0, -0.1),
            GeoPoint(3.0, 5.0),
            GeoPoint(4.0, 6.0),
            GeoPoint(5.0, 7.0),
            GeoPoint(6.0, 8.1),
            GeoPoint(7.0, 9.0),
            GeoPoint(8.0, 9.0),
            GeoPoint(9.0, 9.0)
        )
        val out = RamerDouglasPeucker.process(pointList, 1.0)

        val result = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, -0.1),
            GeoPoint(3.0, 5.0),
            GeoPoint(7.0, 9.0),
            GeoPoint(9.0, 9.0)
        )

        Assert.assertEquals(result, out)
    }
}
