package com.ravenfeld.maps.data.elevation

import junit.framework.TestCase.assertEquals
import org.junit.Test

class ElevationInterpolatorTest {
    @Test
    fun calculateElevationBasedOnTwoPoints() {
        val elevation = ElevationInterpolator.calculateElevationBasedOnTwoPoints(
            1.0,
            1.0,
            1.0,
            0.0,
            10.0,
            1.0,
            2.0,
            20.0
        )
        assertEquals(elevation, 15.0)

        val elevation2 = ElevationInterpolator.calculateElevationBasedOnTwoPoints(
            1.0,
            1.0,
            1.0,
            2.0,
            10.0,
            1.0,
            0.0,
            20.0
        )
        assertEquals(elevation2, 15.0)
    }

    @Test
    fun calculateElevationBasedOnThreePoints() {
        val elevation = ElevationInterpolator.calculateElevationBasedOnThreePoints(
            1.0, 1.0,
            1.0, 0.0, 10.0,
            1.0, 2.0, 20.0,
            3.0, 3.0, 50.0,
        )
        assertEquals(elevation, 15.0)
    }
}
