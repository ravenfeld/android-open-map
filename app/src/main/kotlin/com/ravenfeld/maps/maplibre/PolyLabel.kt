// https://blog.mapbox.com/a-new-algorithm-for-finding-a-visual-center-of-a-polygon-7c77e6492fbc

package com.ravenfeld.maps.maplibre

import org.maplibre.geojson.Point
import org.maplibre.geojson.Polygon
import java.util.PriorityQueue
import kotlin.math.min
import kotlin.math.sqrt

private const val DEFAULT_INITIAL_CAPACITY = 11

@Suppress("LoopWithTooManyJumpStatements", "MagicNumber")
object PolyLabel {
    fun centerForLabel(polygon: Polygon, precision: Double = 0.00005): Point {
        // find the bounding box of the outer ring
        var minX = Double.MAX_VALUE
        var minY = Double.MAX_VALUE
        var maxX = -Double.MAX_VALUE
        var maxY = -Double.MAX_VALUE
        val outerRingCoordinates: List<Point> = polygon.coordinates()[0]
        for (p in outerRingCoordinates) {
            val lon: Double = p.longitude()
            val lat: Double = p.latitude()

            minX = StrictMath.min(minX, lon)
            minY = StrictMath.min(minY, lat)
            maxX = StrictMath.max(maxX, lon)
            maxY = StrictMath.max(maxY, lat)
        }

        val width = maxX - minX
        val height = maxY - minY
        val cellSize = min(width, height)
        var h = cellSize / 2

        if (cellSize == 0.0) return Point.fromLngLat(minX, minY)

        // a priority queue of cells in order of their "potential" (max distance to polygon)
        val cellQueue = PriorityQueue(DEFAULT_INITIAL_CAPACITY, CellComparator())

        // cover polygon with initial cells
        var x = minX
        while (x < maxX) {
            var y = minY
            while (y < maxY) {
                cellQueue.add(Cell(x + h, y + h, h, polygon))
                y += cellSize
            }
            x += cellSize
        }

        // take centroid as the first best guess
        var bestCell = getCentroidCell(polygon)

        // special case for rectangular polygons
        val bboxCell = Cell(minX + width / 2, minY + height / 2, 0.0, polygon)
        if (bboxCell.d > bestCell.d) bestCell = bboxCell

        while (!cellQueue.isEmpty()) {
            // pick the most promising cell from the queue
            val cell = cellQueue.poll() ?: continue

            // update the best cell if we found a better one
            if (cell.d > bestCell.d) {
                bestCell = cell
            }

            // do not drill down further if there's no chance of a better solution
            if (cell.max - bestCell.d <= precision) continue

            // split the cell into four cells
            h = cell.h / 2
            cellQueue.add(Cell(cell.x - h, cell.y - h, h, polygon))
            cellQueue.add(Cell(cell.x + h, cell.y - h, h, polygon))
            cellQueue.add(Cell(cell.x - h, cell.y + h, h, polygon))
            cellQueue.add(Cell(cell.x + h, cell.y + h, h, polygon))
        }

        return Point.fromLngLat(bestCell.x, bestCell.y)
    }

    // get polygon centroid
    private fun getCentroidCell(polygon: Polygon): Cell {
        var area = 0.0
        var x = 0.0
        var y = 0.0

        val points: List<Point> = polygon.coordinates()[0]
        var i = 0
        val len = points.size
        var j = len - 1
        while (i < len) {
            val a: Point = points[i]
            val b: Point = points[j]
            val aLon: Double = a.longitude()
            val aLat: Double = a.latitude()
            val bLon: Double = b.longitude()
            val bLat: Double = b.latitude()

            val f = aLon * bLat - bLon * aLat
            x += (aLon + bLon) * f
            y += (aLat + bLat) * f
            area += f * 3
            j = i++
        }

        if (area == 0.0) {
            val p: Point = points[0]
            return Cell(p.longitude(), p.latitude(), 0.0, polygon)
        }

        return Cell(x / area, y / area, 0.0, polygon)
    }

    private class CellComparator : Comparator<Cell> {
        override fun compare(o1: Cell, o2: Cell): Int {
            return o2.max.compareTo(o1.max)
        }
    }

    private class Cell( // cell center x
        val x: Double, // cell center y
        val y: Double, // half the cell size
        val h: Double,
        polygon: Polygon
    ) {
        // distance from cell center to polygon
        val d: Double

        // max distance to polygon within a cell
        val max: Double

        init {
            this.d = pointToPolygonDist(x, y, polygon)
            this.max = this.d + this.h * sqrt(2.0)
        }

        // signed distance from point to polygon outline (negative if point is outside)
        fun pointToPolygonDist(x: Double, y: Double, polygon: Polygon): Double {
            var inside = false
            var minDistSq = Double.MAX_VALUE

            for (ring in polygon.coordinates()) {
                var i = 0
                val len = ring.size
                var j = len - 1
                while (i < len) {
                    val a: Point = ring[i]
                    val b: Point = ring[j]
                    val aLon: Double = a.longitude()
                    val aLat: Double = a.latitude()
                    val bLon: Double = b.longitude()
                    val bLat: Double = b.latitude()

                    if ((aLat > y != bLat > y) &&
                        (x < (bLon - aLon) * (y - aLat) / (bLat - aLat) + aLon)
                    ) {
                        inside = !inside
                    }

                    minDistSq = min(minDistSq, getSegDistSq(x, y, a, b))
                    j = i++
                }
            }

            return (if (inside) 1 else -1) * sqrt(minDistSq)
        }

        // get squared distance from a point to a segment
        fun getSegDistSq(px: Double, py: Double, a: Point, b: Point): Double {
            var x: Double = a.longitude()
            var y: Double = a.latitude()
            var dx: Double = b.longitude() - x
            var dy: Double = b.latitude() - y

            if (dx != 0.0 || dy != 0.0) {
                val t = ((px - x) * dx + (py - y) * dy) / (dx * dx + dy * dy)

                if (t > 1) {
                    x = b.longitude()
                    y = b.latitude()
                } else if (t > 0) {
                    x += dx * t
                    y += dy * t
                }
            }

            dx = px - x
            dy = py - y

            return dx * dx + dy * dy
        }
    }
}
