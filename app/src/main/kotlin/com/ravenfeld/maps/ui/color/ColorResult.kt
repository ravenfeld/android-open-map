package com.ravenfeld.maps.ui.color

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ColorResult(val trackId: Int, val color: String) : Parcelable
