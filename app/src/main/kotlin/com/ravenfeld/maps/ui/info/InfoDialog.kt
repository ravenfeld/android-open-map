package com.ravenfeld.maps.ui.info

import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.DialogInfoBinding
import com.ravenfeld.maps.ui.common.RequestEvent
import com.ravenfeld.maps.ui.common.alert.AlertDialog.Companion.STATE_CLICK_TYPE_KEY
import com.ravenfeld.maps.ui.common.alert.AlertDialogDirections
import com.ravenfeld.maps.ui.common.alert.AlertResult
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.common.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

private const val MAX_CLICK_SUPER_FRIEND = 5
private const val MIN_CLICK_SUPER_FRIEND = 3
private const val INFO_SUPER_FRIEND = "INFO_SUPER_FRIEND"

@AndroidEntryPoint
class InfoDialog : AppCompatDialogFragment(), InfoPresenter {

    private var _binding: DialogInfoBinding? = null
    private val binding by lazy {
        _binding.getOrThrow()
    }

    private val viewModel: InfoViewModel by viewModels()

    private var navBackStackEntry: NavBackStackEntry? = null

    private val observerOnCreate = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_CREATE) {
            navBackStackEntry?.savedStateHandle?.let { savedStateHandle ->
                savedStateHandle.getLiveData<AlertResult>(STATE_CLICK_TYPE_KEY)
                    .observe(viewLifecycleOwner) { result ->
                        if (result.tag == INFO_SUPER_FRIEND) {
                            findNavController().navigateUp()
                        }
                    }
            }
        }
    }

    private val observerOnDestroy = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            navBackStackEntry?.lifecycle?.removeObserver(observerOnCreate)
            navBackStackEntry?.savedStateHandle?.remove<AlertResult>(STATE_CLICK_TYPE_KEY)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogInfoBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        navBackStackEntry = findNavController().getBackStackEntry(R.id.infoDialog)
        navBackStackEntry?.lifecycle?.addObserver(observerOnCreate)
        viewLifecycleOwner.lifecycle.addObserver(observerOnDestroy)

        viewModel.uiModel(resources.getStringArray(R.array.attributions)).let {
            binding.uiModel = it
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    viewModel.events.collect {
                        it.getContentIfNotHandled()?.let { event ->
                            findNavController().navigateUp()
                            if (event == RequestEvent.Success) {
                                findNavController().safeNavigate(
                                    AlertDialogDirections.actionToAlertOk(
                                        INFO_SUPER_FRIEND,
                                        getString(R.string.super_friend)
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }

    override fun onInfoClick(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun onClickSuperFriend() {
        binding.uiModel?.let {
            it.clickSuperFriend++
            if (it.clickSuperFriend >= MAX_CLICK_SUPER_FRIEND) {
                viewModel.setSuperFriend()
            } else if (it.clickSuperFriend >= MIN_CLICK_SUPER_FRIEND) {
                Toast.makeText(
                    requireContext(),
                    getString(
                        R.string.super_friend_become,
                        MAX_CLICK_SUPER_FRIEND - it.clickSuperFriend
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
