package com.ravenfeld.maps.ui.preset

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.R
import kotlinx.collections.immutable.ImmutableList

@Composable
fun ConfigPresetsScreen(
    viewModel: PresetsViewModel,
    onBack: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val uiState by viewModel.uiState.collectAsState()
    ConfigPresetsContent(
        modifier = modifier,
        presets = uiState.presets,
        onAddPreset = viewModel::onShowAddPreset,
        onSelect = viewModel::onSelect,
        onRename = viewModel::onRename,
        onDelete = viewModel::onDelete,
        onBack = onBack,
    )

    if (uiState.showAddDialog) {
        TextInputDialog(
            onDismissRequest = viewModel::onDismissAddPreset,
            onConfirmed = { viewModel.onAddPreset(it) },
            title = { Text(stringResource(R.string.presets_preset_add)) },
            textInputLabel = { Text(stringResource(R.string.presets_preset_name)) }
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun ConfigPresetsContent(
    presets: ImmutableList<PresetUiModel>,
    onAddPreset: () -> Unit,
    onSelect: (id: Int) -> Unit,
    onRename: (id: Int, name: String) -> Unit,
    onDelete: (id: Int) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Scaffold(
        modifier = modifier,
        topBar = {
            TopAppBar(
                title = { Text(stringResource(R.string.manage_presets)) },
                navigationIcon = {
                    IconButton(
                        onClick = { onBack() },
                    ) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.primary,
                        )
                    }
                },
            )
        },
        content = {
            Box(Modifier.fillMaxHeight().padding(it)) {
                PresetsList(
                    presets = presets,
                    onSelect = onSelect,
                    onRename = onRename,
                    onDelete = onDelete
                )
                FloatingActionButton(
                    onClick = onAddPreset,
                    modifier = Modifier.align(Alignment.BottomEnd).padding(16.dp)
                ) {
                    Icon(
                        imageVector = Icons.Filled.Add,
                        contentDescription = null,
                    )
                }
            }
        }
    )
}

@Composable
private fun PresetsList(
    presets: ImmutableList<PresetUiModel>,
    onSelect: (id: Int) -> Unit,
    onRename: (id: Int, name: String) -> Unit,
    onDelete: (id: Int) -> Unit,
    modifier: Modifier = Modifier,
) {
    LazyColumn(modifier = modifier) {
        item {
            PresetsHeader()
        }
        itemsIndexed(presets) { index, item ->
            if (index > 0) {
                HorizontalDivider()
            }
            PresetItem(
                preset = item,
                onSelect = onSelect,
                onRename = onRename,
                onDelete = onDelete,
                modifier = Modifier.padding(vertical = 4.dp)
            )
        }
    }
}

@Composable
private fun PresetsHeader(
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp)
        ) {
            Text(
                text = stringResource(R.string.presets_preset_name),
                modifier = Modifier.weight(1f),
                style = MaterialTheme.typography.titleMedium
            )
            Text(
                text = stringResource(R.string.presets_selected),
                style = MaterialTheme.typography.titleMedium
            )
        }
    }
}
