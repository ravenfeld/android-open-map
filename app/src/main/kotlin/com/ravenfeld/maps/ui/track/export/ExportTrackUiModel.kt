package com.ravenfeld.maps.ui.track.export

import androidx.databinding.BaseObservable
import com.ravenfeld.maps.ui.common.TextError

class ExportTrackUiModel(originalName: String?, val exportType: ExportType) : BaseObservable() {
    var name: String? = originalName
        set(value) {
            field = value
            nameError = validatorText(value)
            checkError()
            notifyChange()
        }

    var nameError: TextError? = null
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    var isValidate: Boolean = false

    init {
        checkError()
    }

    private fun validatorText(value: String?) = if (value.isNullOrEmpty()) {
        TextError.EMPTY
    } else {
        null
    }

    private fun checkError() {
        isValidate = !when {
            name == null -> true
            nameError != null -> true
            else -> false
        }
    }
}
