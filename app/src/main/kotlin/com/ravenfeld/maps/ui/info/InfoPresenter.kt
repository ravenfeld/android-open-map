package com.ravenfeld.maps.ui.info

interface InfoPresenter {
    fun onInfoClick(url: String)
    fun onClickSuperFriend()
}
