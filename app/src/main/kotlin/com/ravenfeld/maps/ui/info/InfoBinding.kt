package com.ravenfeld.maps.ui.info

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemInfoUiModel(
    items: List<ItemInfoUiModel>?,
    presenter: InfoPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: InfoPresenter
): ListInfoAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListInfoAdapter) {
        recyclerView.adapter as ListInfoAdapter
    } else {
        val bindableRecyclerAdapter = ListInfoAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}
