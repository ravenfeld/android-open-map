package com.ravenfeld.maps.ui.place

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemPlaceOnMapBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemPlaceOnMapUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemPlaceOnMapUiModel,
        newItem: ItemPlaceOnMapUiModel
    ): Boolean = oldItem.placeOnMapUiModel == newItem.placeOnMapUiModel

    override fun areContentsTheSame(
        oldItem: ItemPlaceOnMapUiModel,
        newItem: ItemPlaceOnMapUiModel
    ) = oldItem.placeOnMapUiModel == newItem.placeOnMapUiModel &&
        oldItem.checked == newItem.checked
}

class ListPoiOnMapRecyclerViewAdapter(private val presenter: ItemPlaceOnMapPresenter) :
    ListAdapter<ItemPlaceOnMapUiModel, PoiOnMapViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PoiOnMapViewHolder {
        val binding = ItemPlaceOnMapBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PoiOnMapViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: PoiOnMapViewHolder, position: Int) = holder.bind(getItem(position))
}

class PoiOnMapViewHolder(
    private val binding: ItemPlaceOnMapBinding,
    private val presenter: ItemPlaceOnMapPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemPlaceOnMapUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
