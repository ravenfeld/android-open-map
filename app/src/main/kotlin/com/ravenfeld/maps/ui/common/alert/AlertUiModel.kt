package com.ravenfeld.maps.ui.common.alert

data class AlertUiModel(
    val message: String,
    val buttonRight: String,
    val buttonLeft: String?
)
