package com.ravenfeld.maps.ui.download

import androidx.databinding.BaseObservable
import com.ravenfeld.maps.domain.map.MAX_TILES
import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.getNbTiles
import com.ravenfeld.maps.ui.common.TextError
import org.maplibre.android.geometry.LatLng

class DownloadMapUiModel(val area: Array<LatLng>, val listUiModel: List<ItemDownloadMapUiModel>) : BaseObservable() {

    val onCheckedItem = object : ItemDownloadMapPresenter {
        override fun onCheckedChanged(name: String, checked: Boolean) {
            listUiModel.find { it.name == name }?.checked = checked
            checkError()
            notifyChange()
        }
    }

    var name: String? = null
        set(value) {
            field = value
            nameError = validatorText(value)
            checkError()
            notifyChange()
        }

    var nameError: TextError? = null
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    var isValidate: Boolean = false

    var zoom: IntArray = intArrayOf(9, 15)
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    fun nbTiles(): Int = getNbTiles(area, zoom) * listUiModel.filter { it.checked }.size

    private fun validatorText(value: String?) = if (value.isNullOrEmpty()) {
        TextError.EMPTY
    } else {
        null
    }

    private fun checkError() {
        isValidate = !when {
            name == null -> true
            nameError != null -> true
            MAX_TILES <= nbTiles() -> true
            listUiModel.none { it.checked } -> true
            else -> false
        }
    }
}

fun List<Map>.toListMapDownloadUiModel(area: Array<LatLng>) = DownloadMapUiModel(
    area,
    this.map { tiles ->
        tiles.toItemDownloadMapUiModel()
    }
)
