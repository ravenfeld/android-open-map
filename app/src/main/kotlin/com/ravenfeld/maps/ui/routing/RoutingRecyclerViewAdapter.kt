package com.ravenfeld.maps.ui.routing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemRoutingBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemRoutingUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemRoutingUiModel,
        newItem: ItemRoutingUiModel
    ): Boolean = oldItem.routingMode == newItem.routingMode

    override fun areContentsTheSame(
        oldItem: ItemRoutingUiModel,
        newItem: ItemRoutingUiModel
    ) = oldItem.routingMode == newItem.routingMode
}

class ListRoutingAdapter(private val presenter: RoutingPresenter) :
    ListAdapter<ItemRoutingUiModel, RoutingViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutingViewHolder {
        val binding = ItemRoutingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RoutingViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: RoutingViewHolder, position: Int) =
        holder.bind(getItem(position))
}

class RoutingViewHolder(
    private val binding: ItemRoutingBinding,
    private val presenter: RoutingPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemRoutingUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
