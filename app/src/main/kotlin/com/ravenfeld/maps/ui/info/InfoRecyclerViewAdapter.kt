package com.ravenfeld.maps.ui.info

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemInfoBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemInfoUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemInfoUiModel,
        newItem: ItemInfoUiModel
    ): Boolean = oldItem.title == newItem.title

    override fun areContentsTheSame(
        oldItem: ItemInfoUiModel,
        newItem: ItemInfoUiModel
    ) = oldItem.title == newItem.title &&
        oldItem.url == newItem.url
}

class ListInfoAdapter(private val presenter: InfoPresenter) :
    ListAdapter<ItemInfoUiModel, InfoViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoViewHolder {
        val binding = ItemInfoBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return InfoViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: InfoViewHolder, position: Int) =
        holder.bind(getItem(position))
}

class InfoViewHolder(
    private val binding: ItemInfoBinding,
    private val presenter: InfoPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemInfoUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
