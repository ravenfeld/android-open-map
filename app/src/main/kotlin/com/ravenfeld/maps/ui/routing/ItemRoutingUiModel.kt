package com.ravenfeld.maps.ui.routing

import com.ravenfeld.maps.domain.routing.RoutingMode

data class ItemRoutingUiModel(val routingMode: RoutingMode, val selected: Boolean)
