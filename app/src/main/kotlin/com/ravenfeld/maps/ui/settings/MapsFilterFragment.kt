package com.ravenfeld.maps.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.ravenfeld.maps.databinding.FragmentMapsFilterBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MapsFilterFragment : Fragment(), MapFilterPresenter {

    private var _binding: FragmentMapsFilterBinding? = null
    private val binding get() = _binding.getOrThrow()

    private val viewModel: MapFilterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapsFilterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                requireArguments().getBoolean(FILTER_STREAM).let {
                    viewModel.maps(it).collect { uiModel ->
                        binding.uiModel = uiModel
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onEnableChanged(name: String, enable: Boolean) {
        if (parentFragment is ConfigMapsPresenter) {
            (parentFragment as ConfigMapsPresenter).onEnableChanged(name, enable)
        }
    }

    override fun onEdit(name: String) {
        if (parentFragment is ConfigMapsPresenter) {
            (parentFragment as ConfigMapsPresenter).onEdit(name)
        }
    }

    override fun onDelete(name: String) {
        if (parentFragment is ConfigMapsPresenter) {
            (parentFragment as ConfigMapsPresenter).onDelete(name)
        }
    }

    override fun onExpandedCollapsed(name: String) {
        viewModel.expandedCollapsed(name)
    }

    companion object {
        const val FILTER_STREAM: String = "FILTER_STREAM"
    }
}
