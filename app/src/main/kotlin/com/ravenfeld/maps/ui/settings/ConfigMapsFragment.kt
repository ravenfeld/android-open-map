package com.ravenfeld.maps.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavBackStackEntry
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.FragmentConfigMapsBinding
import com.ravenfeld.maps.ui.common.alert.AlertDialog
import com.ravenfeld.maps.ui.common.alert.AlertDialogDirections
import com.ravenfeld.maps.ui.common.alert.AlertResult
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.common.safeNavigate
import dagger.hilt.android.AndroidEntryPoint

private const val TAG_ALERT_DELETE = "TAG_ALERT_DELETE"

@AndroidEntryPoint
class ConfigMapsFragment : Fragment(), ConfigMapsPresenter {

    private val viewModel: ConfigMapsViewModel by viewModels()

    private var _binding: FragmentConfigMapsBinding? = null
    private val binding get() = _binding.getOrThrow()

    private var navBackStackEntry: NavBackStackEntry? = null

    private val observerOnCreate = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_CREATE) {
            navBackStackEntry?.savedStateHandle?.getLiveData<AlertResult>(AlertDialog.STATE_CLICK_TYPE_KEY)
                ?.observe(viewLifecycleOwner) { result ->
                    findNavController().navigateUp()
                    if (result.tag == TAG_ALERT_DELETE &&
                        result.click == AlertResult.Click.CLICK_RIGHT &&
                        result.content != null
                    ) {
                        viewModel.delete(result.content)
                    }
                }
        }
    }

    private val observerOnDestroy = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            navBackStackEntry?.lifecycle?.removeObserver(observerOnCreate)
            navBackStackEntry?.savedStateHandle?.remove<AlertResult>(AlertDialog.STATE_CLICK_TYPE_KEY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConfigMapsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navBackStackEntry = findNavController().getBackStackEntry(R.id.configMapsFragment)
        navBackStackEntry?.lifecycle?.addObserver(observerOnCreate)
        viewLifecycleOwner.lifecycle.addObserver(observerOnDestroy)
        binding.presenter = this
        binding.viewPager.adapter =
            ConfigMapsChildFragmentStateAdapter(requireContext(), childFragmentManager, viewLifecycleOwner.lifecycle)

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = (binding.viewPager.adapter as ConfigMapsChildFragmentStateAdapter).getTitle(position)
        }.attach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onNavigationBack() {
        findNavController().navigateUp()
    }

    override fun onAddClick() {
        findNavController().safeNavigate(ConfigMapsFragmentDirections.actionToAddUrlMapDialog())
    }

    override fun onEnableChanged(name: String, enable: Boolean) {
        viewModel.enable(name, enable)
    }

    override fun onEdit(name: String) {
        findNavController().safeNavigate(ConfigMapsFragmentDirections.actionToEditMapDialog(name))
    }

    override fun onDelete(name: String) {
        findNavController().safeNavigate(
            AlertDialogDirections.actionToAlertCustom(
                TAG_ALERT_DELETE,
                getString(R.string.delete_map, name),
                getString(android.R.string.ok),
                getString(android.R.string.cancel)
            ).setContent(name)
        )
    }
}
