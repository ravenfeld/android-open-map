package com.ravenfeld.maps.ui.common

enum class TextError { EMPTY, URL_MALFORMED, NAME_EXIST }
