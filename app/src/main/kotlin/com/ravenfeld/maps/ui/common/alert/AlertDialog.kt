package com.ravenfeld.maps.ui.common.alert

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.databinding.DialogAlertBinding
import com.ravenfeld.maps.ui.common.getOrThrow

class AlertDialog : AppCompatDialogFragment(), AlertPresenter {

    private var _binding: DialogAlertBinding? = null

    private val binding by lazy {
        _binding.getOrThrow()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogAlertBinding.inflate(inflater, container, false)

        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        isCancelable = false
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        @SuppressWarnings("MagicNumber")
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog?.window?.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
        binding.uiModel = AlertUiModel(
            AlertDialogArgs.fromBundle(requireArguments()).message,
            AlertDialogArgs.fromBundle(requireArguments()).textButtonRight ?: getString(android.R.string.ok),
            AlertDialogArgs.fromBundle(requireArguments()).textButtonLeft
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onButtonRightClick() {
        onClick(AlertResult.Click.CLICK_RIGHT)
    }

    override fun onButtonLeftClick() {
        onClick(AlertResult.Click.CLICK_LEFT)
    }

    private fun onClick(click: AlertResult.Click) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            STATE_CLICK_TYPE_KEY,
            AlertResult(
                AlertDialogArgs.fromBundle(requireArguments()).tag,
                click,
                AlertDialogArgs.fromBundle(requireArguments()).content
            )
        )
    }

    companion object {
        const val STATE_CLICK_TYPE_KEY = "click_type"
    }
}
