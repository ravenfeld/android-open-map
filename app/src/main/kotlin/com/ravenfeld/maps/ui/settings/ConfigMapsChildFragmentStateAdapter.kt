package com.ravenfeld.maps.ui.settings

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ravenfeld.maps.R

class ConfigMapsChildFragmentStateAdapter(
    val context: Context,
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment = if (position == 0) {
        MapsFilterFragment().apply {
            arguments = bundleOf(
                Pair(
                    MapsFilterFragment.FILTER_STREAM,
                    true
                )
            )
        }
    } else {
        MapsFilterFragment().apply {
            arguments = bundleOf(
                Pair(
                    MapsFilterFragment.FILTER_STREAM,
                    false
                )
            )
        }
    }

    fun getTitle(position: Int): String = if (position == 0) {
        context.getString(R.string.tab_title_map_online)
    } else {
        context.getString(R.string.tab_title_map_offline)
    }
}
