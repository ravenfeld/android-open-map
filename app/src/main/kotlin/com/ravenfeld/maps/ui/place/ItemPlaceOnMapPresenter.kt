package com.ravenfeld.maps.ui.place

interface ItemPlaceOnMapPresenter {
    fun onCheckedChanged(placeOnMapUiModel: PlaceOnMapUiModel, checked: Boolean)
}
