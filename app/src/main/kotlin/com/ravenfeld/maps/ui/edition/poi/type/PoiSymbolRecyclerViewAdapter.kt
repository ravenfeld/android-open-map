package com.ravenfeld.maps.ui.edition.poi.type

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemPoiSymbolBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemPoiSymbolUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemPoiSymbolUiModel,
        newItem: ItemPoiSymbolUiModel
    ): Boolean = oldItem == newItem

    override fun areContentsTheSame(
        oldItem: ItemPoiSymbolUiModel,
        newItem: ItemPoiSymbolUiModel
    ) = oldItem == newItem
}

class ListPoiSymbolAdapter(private val presenter: PoiSymbolPresenter) :
    ListAdapter<ItemPoiSymbolUiModel, PoiSymbolViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PoiSymbolViewHolder {
        val binding = ItemPoiSymbolBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PoiSymbolViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: PoiSymbolViewHolder, position: Int) =
        holder.bind(getItem(position))
}

class PoiSymbolViewHolder(
    private val binding: ItemPoiSymbolBinding,
    private val presenter: PoiSymbolPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemPoiSymbolUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
