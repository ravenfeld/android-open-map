package com.ravenfeld.maps.ui.track.details

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.compose.ui.platform.ViewCompositionStrategy
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.DialogComposeBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.compose.theme.MapsViewerTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.collections.immutable.toImmutableList

@AndroidEntryPoint
class TrackDetailsDialog : AppCompatDialogFragment() {

    private var _binding: DialogComposeBinding? = null
    private val binding get() = _binding.getOrThrow()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = DialogComposeBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.composeView.apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MapsViewerTheme {
                    StatsTrack(
                        statsHighway = TrackDetailsDialogArgs.fromBundle(
                            requireArguments()
                        ).statsHighway.toImmutableList(),
                        statsSurface = TrackDetailsDialogArgs.fromBundle(
                            requireArguments()
                        ).statsSurface.toImmutableList()
                    )
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }
}
