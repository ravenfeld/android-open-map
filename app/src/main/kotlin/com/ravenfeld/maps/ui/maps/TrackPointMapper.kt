package com.ravenfeld.maps.ui.maps

import com.google.gson.JsonObject
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.TrackPoint
import org.maplibre.geojson.Feature
import org.maplibre.geojson.Point

private const val LAT = "LAT"
private const val LNG = "LNG"
private const val ALTITUDE = "ALTITUDE"

internal fun TrackPoint.toFeature() =
    Feature.fromGeometry(
        Point.fromLngLat(geoPoint.longitude, geoPoint.latitude),
        JsonObject().apply {
            addProperty(LAT, geoPoint.latitude)
            addProperty(LNG, geoPoint.longitude)
            addProperty(ALTITUDE, geoPoint.altitude)
        }
    )

internal fun JsonObject.toGeoPoint() =
    GeoPoint(
        latitude = this.get(LAT).asDouble,
        longitude = this.get(LNG).asDouble,
        altitude = this.get(ALTITUDE)?.asDouble
    )
