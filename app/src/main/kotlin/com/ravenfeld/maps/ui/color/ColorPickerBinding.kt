package com.ravenfeld.maps.ui.color

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import top.defaults.colorpicker.ColorPickerView

@BindingAdapter("colorListener")
fun ColorPickerView.bindOnColorChangedListener(listener: OnColorChangedListener) {
    this.subscribe { color, _, _ -> listener.onColorChanged(color) }
}

interface OnColorChangedListener {
    fun onColorChanged(color: Int)
}

@BindingAdapter("color")
fun ImageView.bindColor(color: Int) {
    setColorFilter(color)
}
