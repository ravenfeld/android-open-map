package com.ravenfeld.maps.ui.common.alert

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AlertResult(val tag: String, val click: Click, val content: String? = null) : Parcelable {
    enum class Click {
        CLICK_RIGHT,
        CLICK_LEFT
    }
}
