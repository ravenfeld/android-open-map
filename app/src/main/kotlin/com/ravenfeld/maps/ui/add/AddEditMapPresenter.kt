package com.ravenfeld.maps.ui.add

interface AddEditMapPresenter {
    fun onValidateClick(mode: DialogMode)
}
