package com.ravenfeld.maps.ui.edition.poi

import androidx.databinding.BaseObservable
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.ui.common.TextError

class EditPoiUiModel(
    val mode: PoiDialogMode,
    name: String? = null,
    symbol: PoiSymbol
) : BaseObservable() {
    var name: String? = name
        set(value) {
            field = value
            nameError = validatorText(value)
            checkError()
            notifyChange()
        }
    var symbol: PoiSymbol = symbol
        set(value) {
            field = value
            notifyChange()
        }

    var nameError: TextError? = null
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    var isValidate: Boolean = false

    private fun validatorText(value: String?) = if (value.isNullOrEmpty()) {
        TextError.EMPTY
    } else {
        null
    }

    private fun checkError() {
        isValidate = !when {
            name == null -> true
            nameError != null -> true
            else -> false
        }
    }
}
