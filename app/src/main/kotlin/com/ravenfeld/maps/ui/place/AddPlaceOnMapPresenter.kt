package com.ravenfeld.maps.ui.place

interface AddPlaceOnMapPresenter {
    fun onAddPoi()
}
