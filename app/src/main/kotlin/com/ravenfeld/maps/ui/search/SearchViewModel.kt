package com.ravenfeld.maps.ui.search

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.search.model.Suggestion
import com.ravenfeld.maps.domain.search.usecase.ObserveSuggestionsUseCase
import com.ravenfeld.maps.domain.search.usecase.SuggestionsResult
import com.ravenfeld.maps.ui.search.mapper.toUiModel
import com.ravenfeld.maps.ui.search.model.SuggestionItemUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toImmutableList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.maplibre.android.geometry.LatLng
import javax.inject.Inject

private const val SEARCH_DEBOUNCE_TIME = 750L
private val String.Companion.EMPTY get() = ""

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val observeSuggestions: ObserveSuggestionsUseCase
) : ViewModel() {

    private val _event = MutableSharedFlow<SearchEvent>()
    val event = _event.asSharedFlow()

    private var locationUser by mutableStateOf<LatLng?>(null)

    var searchField by mutableStateOf(String.EMPTY)
        private set

    private val isLoading = MutableStateFlow(false)
    private val suggestionsSaved = MutableStateFlow<List<Suggestion>>(emptyList())

    private val search = MutableStateFlow(String.EMPTY)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    private val suggestionResult = search
        .debounce(SEARCH_DEBOUNCE_TIME)
        .flatMapLatest { search ->
            observeSuggestions(query = search, locationUser = locationUser)
        }

    val uiState = combine(
        suggestionResult
            .onEachSave()
            .onEachSetLoading(),
        isLoading
    ) { suggestionResult, isLoading ->
        val suggestions = (suggestionResult as? SuggestionsResult.Retrieved)
            ?.suggestions
            ?.map {
                it.toUiModel(locationUser)
            }
            ?.toImmutableList()
            ?: persistentListOf()
        SearchUiState(
            suggestions = suggestions,
            isLoading = isLoading,
            message = when {
                suggestionResult is SuggestionsResult.NoInternet -> SearchMessageError.NoInternet
                suggestionResult is SuggestionsResult.SearchIsTooSmall -> SearchMessageError.SearchIsTooSmall
                searchField.isNotEmpty() &&
                    suggestions.isEmpty() &&
                    !isLoading -> SearchMessageError.NoResults
                else -> null
            }
        )
    }.stateIn(
        scope = viewModelScope,
        started = WhileSubscribedOrRetained,
        initialValue = SearchUiState.start()
    )

    fun init(location: LatLng?) {
        locationUser = location
    }

    private fun Flow<SuggestionsResult>.onEachSetLoading(): Flow<SuggestionsResult> =
        onEach { isLoading.emit(it is SuggestionsResult.Loading) }

    private fun Flow<SuggestionsResult>.onEachSave(): Flow<SuggestionsResult> =
        onEach {
            if (it is SuggestionsResult.Retrieved) {
                suggestionsSaved.emit(it.suggestions)
            }
        }

    fun onBackClicked() {
        viewModelScope.launch {
            _event.emit(SearchEvent.OnBackClicked)
        }
    }

    fun onSearchSelected(suggestionId: Long) {
        viewModelScope.launch {
            suggestionsSaved
                .value
                .find { it.id == suggestionId }
                ?.let { suggestion ->
                    _event.emit(
                        SearchEvent.SuggestionSelected(
                            suggestion =
                            SearchResult(
                                center =
                                suggestion.center,
                                bounds = suggestion.bounds
                            )
                        )
                    )
                }
        }
    }

    fun onSearchChanged(search: String) {
        searchField = search
        this.isLoading.update { search.isNotEmpty() }
        this.search.update { search }
    }

    fun onSearchClose() {
        searchField = ""
        search.update { "" }
    }
}

@Immutable
data class SearchUiState(
    val suggestions: ImmutableList<SuggestionItemUiModel>,
    val isLoading: Boolean,
    val message: SearchMessageError?
) {

    companion object {
        fun start() = SearchUiState(
            suggestions = persistentListOf(),
            isLoading = false,
            message = null,
        )
    }
}

sealed interface SearchEvent {
    data class SuggestionSelected(val suggestion: SearchResult) : SearchEvent
    data object OnBackClicked : SearchEvent
}

sealed interface SearchMessageError {
    data object NoResults : SearchMessageError
    data object NoInternet : SearchMessageError
    data object SearchIsTooSmall : SearchMessageError
}
