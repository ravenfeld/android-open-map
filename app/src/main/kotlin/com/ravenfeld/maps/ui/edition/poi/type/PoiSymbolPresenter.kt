package com.ravenfeld.maps.ui.edition.poi.type

import com.ravenfeld.maps.domain.geo.PoiSymbol

interface PoiSymbolPresenter {
    fun onPoiSymbolSelected(symbol: PoiSymbol)
}
