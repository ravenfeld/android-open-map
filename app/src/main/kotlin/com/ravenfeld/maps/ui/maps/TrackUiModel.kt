package com.ravenfeld.maps.ui.maps

import android.graphics.Color
import com.ravenfeld.maps.data.mapper.toPoint
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.domain.geo.Track
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.TrackStats
import com.ravenfeld.maps.domain.geo.toPoint
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.style.expressions.Expression
import org.maplibre.android.style.expressions.Expression.all
import org.maplibre.android.style.expressions.Expression.eq
import org.maplibre.android.style.expressions.Expression.get
import org.maplibre.android.style.expressions.Expression.literal
import org.maplibre.android.style.expressions.Expression.switchCase
import org.maplibre.android.style.layers.CircleLayer
import org.maplibre.android.style.layers.LineLayer
import org.maplibre.android.style.layers.Property
import org.maplibre.android.style.layers.PropertyFactory
import org.maplibre.android.style.layers.SymbolLayer
import org.maplibre.android.style.sources.GeoJsonSource
import org.maplibre.geojson.Feature
import org.maplibre.geojson.FeatureCollection
import org.maplibre.geojson.LineString

const val SOURCE_LINE = "SOURCE_LINE_TRACK_"
const val LAYER_LINE = "LAYER_LINE_TRACK_"
const val LAYER_ARROW = "LAYER_ARROW_TRACK_"
const val SOURCE_CIRCLE = "SOURCE_CIRCLE"
const val LAYER_CIRCLE = "LAYER_CIRCLE_TRACK_"
const val SOURCE_POI = "SOURCE_POI_TRACK_"
const val LAYER_POI_ON_TRACK = "LAYER_POI_ON_TRACK_"

@Suppress("LongParameterList")
class TrackUiModel(
    val id: Int,
    val name: String,
    val visible: Boolean,
    val color: String,
    val stats: TrackStats,
    val trackPoints: List<TrackPoint>,
    val waypoints: List<GeoPoint>,
    val pois: List<Poi>,
    val locationNorthEast: LatLng,
    val locationSouthWest: LatLng
) {

    val sourceLineName = SOURCE_LINE + id
    val layerLineName = LAYER_LINE + id
    val layerArrowName = LAYER_ARROW + id
    val sourceCircleName = SOURCE_CIRCLE + id
    val layerCircleName = LAYER_CIRCLE + id
    val sourcePoiName = SOURCE_POI + id
    val layerPoiName = LAYER_POI_ON_TRACK + id

    fun toLineSource() =
        GeoJsonSource(
            SOURCE_LINE + id,
            Feature.fromGeometry(
                LineString.fromLngLats(trackPoints.map { it.toPoint() })
            )
        )

    @Suppress("MagicNumber")
    fun toLineLayer(color: String) = LineLayer(
        layerLineName,
        sourceLineName
    )
        .withProperties(
            PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
            PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
            PropertyFactory.lineWidth(6.5f),
            PropertyFactory.lineColor(Color.parseColor(color))
        )

    @Suppress("MagicNumber")
    fun toArrowLayer(arrowImage: String) =
        SymbolLayer(layerArrowName, sourceLineName)
            .withProperties(
                PropertyFactory.symbolPlacement(Property.SYMBOL_PLACEMENT_LINE),
                PropertyFactory.symbolSpacing(50.0f),
                PropertyFactory.iconImage(arrowImage),
                PropertyFactory.iconSize(0.65f),
                PropertyFactory.iconAllowOverlap(true)
            ).apply {
                minZoom = 12.0f
            }

    fun toCircleSource() =
        GeoJsonSource(
            sourceCircleName,
            Feature.fromGeometry(
                trackPoints.last().toPoint()
            )
        )

    @Suppress("MagicNumber")
    fun toCircleLayer(color: String) =
        CircleLayer(layerCircleName, sourceCircleName)
            .withProperties(
                PropertyFactory.circleRadius(6f),
                PropertyFactory.circleColor(Color.parseColor(color))
            )

    fun toPoiOnTrackSource() =
        GeoJsonSource(
            sourcePoiName,
            FeatureCollection.fromFeatures(
                pois.map {
                    it.toFeature()
                }
            )
        )

    fun toPoiOnTrackLayer() =
        SymbolLayer(layerPoiName, sourcePoiName)
            .withProperties(
                PropertyFactory.iconImage(getPoiOnTrackImageExpression()),
                PropertyFactory.iconAllowOverlap(true),
                PropertyFactory.iconAnchor(Property.ICON_ANCHOR_CENTER),
            )

    private fun getPoiOnTrackImageExpression(): Expression {
        val expressions = mutableListOf<Expression>()
        PoiSymbol.entries.forEach {
            expressions.add(
                all(
                    eq(
                        get(literal(POI_TYPE)),
                        literal(it.name)
                    )
                )
            )
            expressions.add(literal(POI_ON_TRACK_IMAGE + it.name))
        }
        expressions.add(literal(POI_ON_TRACK_IMAGE + PoiSymbol.GENERIC.name))
        @Suppress("SpreadOperator")
        return switchCase(*expressions.toTypedArray())
    }
}

fun Track.toTrackUiModel() =
    TrackUiModel(
        id = id,
        name = name,
        visible = visible,
        color = color,
        stats = stats,
        trackPoints = trackPoints,
        waypoints = coordinate.waypoints.minus(
            coordinate.pois.map {
                it.point
            }.toSet()
        ),
        pois = coordinate.pois,
        locationNorthEast = LatLng(locationNorthEast.latitude, locationNorthEast.longitude),
        locationSouthWest = LatLng(locationSouthWest.latitude, locationSouthWest.longitude)
    )
