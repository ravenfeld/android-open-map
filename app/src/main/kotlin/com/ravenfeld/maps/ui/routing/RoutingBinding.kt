package com.ravenfeld.maps.ui.routing

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.ui.common.ItemDecoration

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemRoutingUiModel(
    items: List<ItemRoutingUiModel>?,
    presenter: RoutingPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: RoutingPresenter
): ListRoutingAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListRoutingAdapter) {
        recyclerView.adapter as ListRoutingAdapter
    } else {
        val bindableRecyclerAdapter = ListRoutingAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, false)
            )
        }
        bindableRecyclerAdapter
    }
}

@BindingAdapter("routingIcon")
fun ImageView.setRoutingIcon(routingMode: RoutingMode) {
    when (routingMode) {
        RoutingMode.FREE -> setImageResource(R.drawable.icon_routes_free)
        RoutingMode.HIKE -> setImageResource(R.drawable.icon_routes_hike)
        RoutingMode.MOUNTAIN_BIKE -> setImageResource(R.drawable.icon_routes_mountainbike)
        RoutingMode.ROAD_BIKE -> setImageResource(R.drawable.icon_routes_roadbike)
    }
}

@BindingAdapter("routingTitle")
fun TextView.setRoutingTitle(routingMode: RoutingMode) {
    when (routingMode) {
        RoutingMode.FREE -> setText(R.string.routes_free_title)
        RoutingMode.HIKE -> setText(R.string.routes_hike_title)
        RoutingMode.MOUNTAIN_BIKE -> setText(R.string.routes_mountainbike_title)
        RoutingMode.ROAD_BIKE -> setText(R.string.routes_roadbike_title)
    }
}

@BindingAdapter("routingSubtitle")
fun TextView.setRoutingSubtitle(routingMode: RoutingMode) {
    when (routingMode) {
        RoutingMode.FREE -> setText(R.string.routes_free_subtitle)
        RoutingMode.HIKE -> setText(R.string.routes_hike_subtitle)
        RoutingMode.MOUNTAIN_BIKE -> setText(R.string.routes_mountainbike_subtitle)
        RoutingMode.ROAD_BIKE -> setText(R.string.routes_roadbike_subtitle)
    }
}

@BindingAdapter("routingSelected")
fun View.setRoutingSelected(selected: Boolean) {
    if (selected) {
        setBackgroundColor(ContextCompat.getColor(context, R.color.grey))
    } else {
        background = null
    }
}
