package com.ravenfeld.maps.ui.edition

interface AddOnTrackDialogPresenter {
    fun addWaypoint()
    fun addPoi()
}
