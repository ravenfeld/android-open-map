package com.ravenfeld.maps.ui.maps

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemTrackBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemTrackUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemTrackUiModel,
        newItem: ItemTrackUiModel
    ): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: ItemTrackUiModel,
        newItem: ItemTrackUiModel
    ) = oldItem == newItem
}

class ListTrackAdapter(private val presenter: MapPresenter) :
    ListAdapter<ItemTrackUiModel, TrackViewHolder>(DIFF_UTILS) {

    var listOriginal: List<ItemTrackUiModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val binding = ItemTrackBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return TrackViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun submitList(list: List<ItemTrackUiModel>?) {
        super.submitList(list)
        listOriginal = list
    }
}

class TrackViewHolder(
    private val binding: ItemTrackBinding,
    private val presenter: MapPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemTrackUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
