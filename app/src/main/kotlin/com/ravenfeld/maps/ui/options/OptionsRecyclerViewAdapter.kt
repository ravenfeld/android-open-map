package com.ravenfeld.maps.ui.options

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemOptionBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<Option>() {

    override fun areItemsTheSame(
        oldItem: Option,
        newItem: Option
    ): Boolean = oldItem == newItem

    override fun areContentsTheSame(
        oldItem: Option,
        newItem: Option
    ) = oldItem == newItem
}

class ListOptionAdapter(private val presenter: OptionsPresenter) :
    ListAdapter<Option, OptionViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {
        val binding = ItemOptionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return OptionViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) =
        holder.bind(getItem(position))
}

class OptionViewHolder(
    private val binding: ItemOptionBinding,
    private val presenter: OptionsPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: Option) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
