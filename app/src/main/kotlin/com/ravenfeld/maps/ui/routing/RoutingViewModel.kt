package com.ravenfeld.maps.ui.routing

import androidx.lifecycle.ViewModel
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.domain.settings.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RoutingViewModel @Inject constructor(
    private val settingsRepository: SettingsRepository
) : ViewModel() {

    val uiModel = RoutingUiModel(settingsRepository.getRoutingMode())

    fun setRoutingMode(routingMode: RoutingMode) = settingsRepository.setRoutingMode(routingMode)
}
