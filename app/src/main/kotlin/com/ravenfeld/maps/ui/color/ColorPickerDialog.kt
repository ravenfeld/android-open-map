package com.ravenfeld.maps.ui.color

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.databinding.DialogColorPickerBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ColorPickerDialog : AppCompatDialogFragment(), ColorPickerPresenter {

    private var _binding: DialogColorPickerBinding? = null
    private val binding by lazy {
        _binding.getOrThrow()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DialogColorPickerBinding.inflate(inflater, container, false)

        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        @SuppressWarnings("MagicNumber")
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog?.window?.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
        binding.uiModel = ColorPickerUiModel().apply {
            indicatorColor = ColorPickerDialogArgs.fromBundle(requireArguments()).colorDefault
            binding.colorPicker.setInitialColor(indicatorColor)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onColorChanged(color: Int) {
        binding.uiModel?.indicatorColor = color
    }

    @Suppress("MagicNumber")
    override fun onButtonOkClick() {
        binding.uiModel?.indicatorColor?.let { color ->
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                STATE_COLOR_SELECTED,
                ColorResult(
                    ColorPickerDialogArgs.fromBundle(requireArguments()).trackId,
                    String.format(Locale.getDefault(), "#%08X", 0xFFFFFFFF and color.toLong())
                )

            )
        }
    }

    override fun onButtonCancelClick() {
        dismiss()
    }

    companion object {
        const val STATE_COLOR_SELECTED = "color_selected"
    }
}
