package com.ravenfeld.maps.ui.search

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.geometry.LatLngBounds

@Parcelize
data class SearchResult(
    val center: LatLng,
    val bounds: LatLngBounds?
) : Parcelable
