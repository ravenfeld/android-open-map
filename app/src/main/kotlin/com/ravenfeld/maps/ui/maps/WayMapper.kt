package com.ravenfeld.maps.ui.maps

import com.google.gson.JsonObject
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Tag
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.Way
import com.ravenfeld.maps.domain.geo.toPoint
import org.maplibre.geojson.Feature
import org.maplibre.geojson.LineString
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMisc

private const val START_LAT = "START_LAT"
private const val START_LNG = "START_LNG"
private const val END_LAT = "END_LAT"
private const val END_LNG = "END_LNG"
private const val START_DISTANCE = "START_DISTANCE"
private const val END_DISTANCE = "END_DISTANCE"

internal fun Way.toFeature(trackPoints: List<TrackPoint>): Feature {
    val lineSlice =
        TurfMisc.lineSliceAlong(
            LineString.fromLngLats(trackPoints.toPoint()),
            startDistance,
            endDistance,
            TurfConstants.UNIT_METERS
        )
    return Feature.fromGeometry(
        lineSlice,
        JsonObject().apply {
            addProperty(START_LAT, start.latitude)
            addProperty(START_LNG, start.longitude)
            addProperty(END_LAT, end.latitude)
            addProperty(END_LNG, end.longitude)
            addProperty(START_DISTANCE, startDistance)
            addProperty(END_DISTANCE, endDistance)
        }
    )
}

internal fun JsonObject.toWay() =
    Way(
        start = GeoPoint(
            latitude = this.get(START_LAT).asDouble,
            longitude = this.get(START_LNG).asDouble
        ),
        startDistance = this.get(START_DISTANCE).asDouble,
        end = GeoPoint(
            latitude = this.get(END_LAT).asDouble,
            longitude = this.get(END_LNG).asDouble
        ),
        endDistance = this.get(END_DISTANCE).asDouble,
        tag = Tag()
    )
