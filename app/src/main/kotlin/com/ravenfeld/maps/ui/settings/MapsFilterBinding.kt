package com.ravenfeld.maps.ui.settings

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.ui.common.ItemDecoration

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemMapFilterBaseUiModel(
    items: List<ItemMapFilterBaseUiModel>?,
    presenter: MapFilterPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: MapFilterPresenter
): ListMapFilterRecyclerViewAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListMapFilterRecyclerViewAdapter) {
        recyclerView.adapter as ListMapFilterRecyclerViewAdapter
    } else {
        val bindableRecyclerAdapter = ListMapFilterRecyclerViewAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, true)
            )
        }
        bindableRecyclerAdapter
    }
}

@BindingAdapter("name")
fun TextView.setName(uiModel: ItemSubMapFilterUiModel) {
    text = uiModel.name.replace(uiModel.groupName + " ", "")
}
