package com.ravenfeld.maps.ui.track.export

enum class ExportType {
    GPX, FIT
}
