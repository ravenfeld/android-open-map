package com.ravenfeld.maps.ui.maps

import com.google.gson.JsonObject
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.PoiSymbol
import org.maplibre.geojson.Feature
import org.maplibre.geojson.Point

internal const val POI_TYPE = "POI_TYPE"

private const val NAME = "NAME"
private const val LAT = "LAT"
private const val LNG = "LNG"

internal fun Poi.toFeature() =
    Feature.fromGeometry(
        Point.fromLngLat(point.longitude, point.latitude),
        JsonObject().apply {
            addProperty(NAME, name)
            addProperty(POI_TYPE, symbol.name)
            addProperty(LAT, point.latitude)
            addProperty(LNG, point.longitude)
        }
    )

internal fun JsonObject.toPoi() =
    Poi(
        name = this.get(NAME).asString,
        point = GeoPoint(
            this.get(LAT).asDouble,
            this.get(LNG).asDouble
        ),
        symbol = PoiSymbol.valueOf(this.get(POI_TYPE).asString)
    )
