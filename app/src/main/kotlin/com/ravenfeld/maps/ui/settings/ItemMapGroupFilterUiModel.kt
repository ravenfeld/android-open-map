package com.ravenfeld.maps.ui.settings

import com.ravenfeld.maps.domain.map.MapGroup
import com.ravenfeld.maps.domain.map.MapStorage

class ItemMapGroupFilterUiModel(
    name: String,
    storage: MapStorage,
    enable: Boolean,
    var expand: Boolean
) : ItemMapFilterBaseUiModel(name, storage, enable) {

    fun copy(): ItemMapGroupFilterUiModel = ItemMapGroupFilterUiModel(name, storage, enable, expand)
}

fun MapGroup.toItemMapGroupFilterUiModel(expand: Boolean) = ItemMapGroupFilterUiModel(
    name,
    storage,
    enable,
    expand
)
