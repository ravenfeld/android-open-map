package com.ravenfeld.maps.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.ravenfeld.maps.data.database.PresetEntity
import com.ravenfeld.maps.data.database.PresetNameUpdateEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PresetDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(preset: PresetEntity): Long

    @Query("DELETE FROM preset WHERE id = :id")
    suspend fun delete(id: Int)

    @Update(entity = PresetEntity::class)
    suspend fun update(preset: PresetNameUpdateEntity)

    @Query("SELECT * FROM preset ORDER BY id ASC")
    fun getPresets(): Flow<List<PresetEntity>>

    @Query("SELECT * FROM preset ORDER BY id ASC")
    fun getPresetsSync(): List<PresetEntity>

    @Query("SELECT * FROM preset WHERE id = :id")
    suspend fun getPreset(id: Int): PresetEntity
}
