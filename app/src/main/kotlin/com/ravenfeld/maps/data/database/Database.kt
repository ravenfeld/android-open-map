package com.ravenfeld.maps.data.database

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.DeleteTable
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.AutoMigrationSpec
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ravenfeld.maps.data.database.dao.MapDao
import com.ravenfeld.maps.data.database.dao.PresetDao
import com.ravenfeld.maps.data.database.dao.PresetMapDao
import com.ravenfeld.maps.data.database.dao.TrackDao
import com.ravenfeld.maps.data.database.dao.UserActionDao
import com.ravenfeld.maps.domain.track.UserActionEntity

/**
 * Class that defines the database room.
 */
@Database(
    entities = [
        MapEntity::class,
        TrackEntity::class,
        UserActionEntity::class,
        PresetEntity::class,
        PresetMapEntity::class
    ],
    version = 9,
    autoMigrations = [
        AutoMigration(from = 5, to = 6, spec = DatabaseAutoMigration5to6::class),
        AutoMigration(from = 6, to = 7)
    ],
    exportSchema = true
)
@TypeConverters(DateConverter::class, UserActionConverter::class)
abstract class Database : RoomDatabase() {

    abstract fun mapDao(): MapDao

    abstract fun trackDao(): TrackDao

    abstract fun userActionDao(): UserActionDao

    abstract fun presetDao(): PresetDao

    abstract fun presetMapDao(): PresetMapDao

    companion object {
        /**
         * Name of the database.
         */
        const val DATABASE_NAME: String = "database"
    }
}

@DeleteTable(tableName = "route")
class DatabaseAutoMigration5to6 : AutoMigrationSpec {
    @Override
    override fun onPostMigrate(db: SupportSQLiteDatabase) {
        // Invoked once auto migration is done
    }
}
