package com.ravenfeld.maps.data.repository

import com.ravenfeld.maps.BuildConfig
import com.ravenfeld.maps.data.remote.OverpassWebService
import com.ravenfeld.maps.data.remote.dto.LatLngDto
import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.place.repository.PlaceOnMapRepository
import com.ravenfeld.maps.maplibre.PolyLabel
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.geometry.LatLngBounds
import org.maplibre.geojson.Point
import org.maplibre.geojson.Polygon
import javax.inject.Inject

class PlaceOnMapRepositoryImpl @Inject constructor(
    private val service: OverpassWebService
) : PlaceOnMapRepository {
    override suspend fun getPlaceOnMap(type: Type, area: LatLngBounds): List<PlaceOnMap> =
        service.getPlace(type, area).map { response ->
            response.elements.mapNotNull {
                if (it.latitude != null && it.longitude != null) {
                    PlaceOnMap(
                        center = LatLng(it.latitude, it.longitude),
                        type = type,
                        text = it.tags?.toText()
                    )
                } else if (it.geometry != null) {
                    PlaceOnMap(
                        center = it.geometry.toCenter(),
                        type = type,
                        text = it.tags?.toText()
                    )
                } else {
                    null
                }
            }
        }.getOrNull() ?: emptyList()
}

private fun Map<String, String>.toText() =
    if (BuildConfig.DEBUG) {
        toList().joinToString("\n") {
            "${it.first}=${it.second}"
        }
    } else {
        get("ele")?.let {
            "${get("name")}\n( $it m )"
        } ?: get("name")
    }

private fun List<LatLngDto>.toCenter(): LatLng {
    val polygon = Polygon.fromLngLats(listOf(this.map { Point.fromLngLat(it.longitude, it.latitude) }))
    return PolyLabel.centerForLabel(polygon).run {
        LatLng(latitude(), longitude())
    }
}
