package com.ravenfeld.maps.data.routing

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream

@Suppress("DataClassShouldBeImmutable")
data class GridInfo(val gridInfoFile: File?) {

    // Indicates if grid data is loaded
    var isLoaded: Boolean = false

    var gridRaster = 0f
    var gridMinLat = 0f
    var gridMinLon = 0f
    var gridLatCount = 0
    var gridLonCount = 0
    var gridIndices: Array<IntArray> = arrayOf()

    /**
     * Creates loaded grid info
     */
    init {
        var gridReader: ObjectInputStream? = null
        try {
            gridReader = ObjectInputStream(BufferedInputStream(FileInputStream(gridInfoFile)))
            gridRaster = gridReader.readFloat()
            gridMinLat = gridReader.readFloat()
            gridMinLon = gridReader.readFloat()
            gridLatCount = gridReader.readInt()
            gridLonCount = gridReader.readInt()
            gridReader.close()
            gridIndices = Array(gridLatCount) { IntArray(gridLonCount) }

            var gridIndex = 0
            for (iLat in 0 until gridLatCount) {
                for (iLon in 0 until gridLonCount) {
                    gridIndices[iLat][iLon] = gridIndex
                    gridIndex++
                }
            }

            isLoaded = true
        } finally {
            gridReader?.close()
        }
    }
}
