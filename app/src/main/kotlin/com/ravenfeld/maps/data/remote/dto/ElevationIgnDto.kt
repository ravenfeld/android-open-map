package com.ravenfeld.maps.data.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ElevationsIgnDto(
    @SerialName("elevations")
    val elevations: List<ElevationIgnDto>
)

@Serializable
data class ElevationIgnDto(
    @SerialName("lat")
    val latitude: Double,
    @SerialName("lon")
    val longitude: Double,
    @SerialName("z")
    val altitude: Double
)
