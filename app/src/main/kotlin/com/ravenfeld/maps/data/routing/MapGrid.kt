package com.ravenfeld.maps.data.routing

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream

@Suppress("DataClassShouldBeImmutable")
data class MapGrid(val gridIndex: Int, val gridOperationTimestamp: Long, val gridFile: File?) {

    // Indicates if grid data is loaded
    var isLoaded: Boolean = false

    // Timestamp of the last node visit in this array
    var visitTimestamp: Long = gridOperationTimestamp
    val nodeCount: Int
    val nodesLat: FloatArray
    val nodesLon: FloatArray
    val nodesEdgeOffset: IntArray
    val edgeCount: Int
    val edgesTargetNodeGridIndex: LongArray
    val edgesInfobits: ByteArray
    val edgesLengths: FloatArray
    val edgesMaxSpeeds: ByteArray

    /**
     * Creates loaded map grid
     */
    init {
        var gridReader: ObjectInputStream? = null
        try {
            gridReader = ObjectInputStream(BufferedInputStream(FileInputStream(gridFile)))
            nodeCount = gridReader.readInt()
            edgeCount = gridReader.readInt()
            nodesLat = gridReader.readObject() as FloatArray
            nodesLon = gridReader.readObject() as FloatArray
            nodesEdgeOffset = gridReader.readObject() as IntArray
            edgesTargetNodeGridIndex = gridReader.readObject() as LongArray
            edgesInfobits = gridReader.readObject() as ByteArray
            edgesLengths = gridReader.readObject() as FloatArray
            edgesMaxSpeeds = gridReader.readObject() as ByteArray
            gridReader.close()
            isLoaded = true
        } finally {
            gridReader?.close()
        }
    }
}
