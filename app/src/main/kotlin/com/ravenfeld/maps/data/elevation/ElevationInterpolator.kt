package com.ravenfeld.maps.data.elevation

import com.ravenfeld.maps.domain.geo.GeoPoint
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

private const val EPSILON = 0.00001
private const val EPSILON2 = EPSILON * EPSILON

object ElevationInterpolator {
    @Suppress("LongParameterList")
    fun calculateElevationBasedOnTwoPoints(
        lat: Double,
        lon: Double,
        lat0: Double,
        lon0: Double,
        ele0: Double,
        lat1: Double,
        lon1: Double,
        ele1: Double
    ): Double {
        val dlat0 = lat0 - lat
        val dlon0 = lon0 - lon
        val dlat1 = lat1 - lat
        val dlon1 = lon1 - lon
        val l0 = sqrt(dlon0 * dlon0 + dlat0 * dlat0)
        val l1 = sqrt(dlon1 * dlon1 + dlat1 * dlat1)
        val l = l0 + l1
        return if (l < EPSILON) {
            // If points are too close to each other, return elevation of the
            // point which is closer;
            if (l0 <= l1) ele0 else ele1
        } else {
            // Otherwise do linear interpolation
            round(ele0 + (ele1 - ele0) * l0 / l, 2)
        }
    }

    @Suppress("LongParameterList")
    fun calculateElevationBasedOnThreePoints(
        lat: Double,
        lon: Double,
        lat0: Double,
        lon0: Double,
        ele0: Double,
        lat1: Double,
        lon1: Double,
        ele1: Double,
        lat2: Double,
        lon2: Double,
        ele2: Double
    ): Double {
        val dlat10 = lat1 - lat0
        val dlon10 = lon1 - lon0
        val dele10 = ele1 - ele0
        val dlat20 = lat2 - lat0
        val dlon20 = lon2 - lon0
        val dele20 = ele2 - ele0
        val a = dlon10 * dele20 - dele10 * dlon20
        val b = dele10 * dlat20 - dlat10 * dele20
        val c = dlat10 * dlon20 - dlon10 * dlat20
        return if (abs(c) < EPSILON) {
            val dlat21 = lat2 - lat1
            val dlon21 = lon2 - lon1
            val dele21 = ele2 - ele1
            val l10 = dlat10 * dlat10 + dlon10 * dlon10 + dele10 * dele10
            val l20 = dlat20 * dlat20 + dlon20 * dlon20 + dele20 * dele20
            val l21 = dlat21 * dlat21 + dlon21 * dlon21 + dele21 * dele21
            if (l21 > l10 && l21 > l20) {
                calculateElevationBasedOnTwoPoints(
                    lat,
                    lon,
                    lat1,
                    lon1,
                    ele1,
                    lat2,
                    lon2,
                    ele2
                )
            } else if (l20 > l10 && l20 > l21) {
                calculateElevationBasedOnTwoPoints(
                    lat,
                    lon,
                    lat0,
                    lon0,
                    ele0,
                    lat2,
                    lon2,
                    ele2
                )
            } else {
                calculateElevationBasedOnTwoPoints(
                    lat,
                    lon,
                    lat0,
                    lon0,
                    ele0,
                    lat1,
                    lon1,
                    ele1
                )
            }
        } else {
            val d = a * lat0 + b * lon0 + c * ele0
            val ele = (d - a * lat - b * lon) / c
            round(ele, 2)
        }
    }

    @Suppress("MagicNumber")
    fun calculateElevationBasedOnPointList(lat: Double, lon: Double, pointList: List<GeoPoint>): Double {
        // See http://math.stackexchange.com/a/1930758/140512 for the
        // explanation
        val size = pointList.size
        return when (size) {
            0 -> throw IllegalArgumentException("At least one point is required in the pointList.")
            1 -> pointList[0].altitude!!
            2 -> calculateElevationBasedOnTwoPoints(
                lat,
                lon,
                pointList[0].latitude,
                pointList[0].longitude,
                pointList[0].altitude!!,
                pointList[1].latitude,
                pointList[1].longitude,
                pointList[1].altitude!!
            )

            3 -> calculateElevationBasedOnThreePoints(
                lat, lon,
                pointList[0].latitude, pointList[0].longitude, pointList[0].altitude!!,
                pointList[1].latitude, pointList[1].longitude, pointList[1].altitude!!,
                pointList[2].latitude, pointList[2].longitude, pointList[2].altitude!!
            )

            else -> {
                val vs = DoubleArray(size)
                val eles = DoubleArray(size)
                var v = 0.0
                for (index in 0 until size) {
                    val lati = pointList[index].latitude
                    val loni = pointList[index].longitude
                    val dlati = lati - lat
                    val dloni = loni - lon
                    val l2 = dlati * dlati + dloni * dloni
                    eles[index] = pointList[index].altitude!!
                    if (l2 < EPSILON2) {
                        return eles[index]
                    }
                    vs[index] = 1 / l2
                    v += vs[index]
                }
                var ele = 0.0
                for (index in 0 until size) {
                    ele += eles[index] * vs[index] / v
                }
                round(ele, 2)
            }
        }
    }

    private fun round(value: Double, decimalPlaces: Int): Double {
        val factor = 10.0.pow(decimalPlaces.toDouble())
        return (value * factor).roundToInt() / factor
    }
}
