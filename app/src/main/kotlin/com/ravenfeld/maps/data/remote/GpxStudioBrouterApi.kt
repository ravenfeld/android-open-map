package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.ErrorDto
import com.ravenfeld.maps.data.remote.dto.GpxStudioBrouterDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GpxStudioBrouterApi {
    @Headers("Accept: application/json")
    @GET("?alternativeidx=0&format=geojson")
    suspend fun route(
        @Query("lonlats") lonlats: String,
        @Query("profile") profile: String
    ): NetworkResponse<GpxStudioBrouterDto, ErrorDto>
}
