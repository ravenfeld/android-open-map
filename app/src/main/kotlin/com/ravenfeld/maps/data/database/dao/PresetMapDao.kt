package com.ravenfeld.maps.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.ravenfeld.maps.data.database.PresetMapAlphaUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapEnableUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapEntity
import com.ravenfeld.maps.data.database.PresetMapOrderUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapVisibleUpdateEntity
import kotlinx.coroutines.flow.Flow

@Suppress("TooManyFunctions")
@Dao
interface PresetMapDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(preset: PresetMapEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(presets: List<PresetMapEntity>): List<Long>

    @Update(entity = PresetMapEntity::class)
    suspend fun update(map: PresetMapEnableUpdateEntity)

    @Update(entity = PresetMapEntity::class)
    suspend fun update(map: PresetMapAlphaUpdateEntity)

    @Update(entity = PresetMapEntity::class)
    suspend fun update(map: PresetMapVisibleUpdateEntity)

    @Update(entity = PresetMapEntity::class)
    suspend fun update(maps: List<PresetMapOrderUpdateEntity>)

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId AND mapName IS :mapName ")
    suspend fun getPresetMapSync(presetId: Int, mapName: String): PresetMapEntity?

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId")
    suspend fun getPresetMapSync(presetId: Int): List<PresetMapEntity>

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId")
    fun getPresetMap(presetId: Int): Flow<List<PresetMapEntity>>

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId AND enable = 1")
    fun getPresetMapEnable(presetId: Int): Flow<List<PresetMapEntity>>

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId ORDER BY `order`")
    fun getAllOrderSync(presetId: Int): List<PresetMapEntity>

    @Query("SELECT * FROM preset_map WHERE presetId =:presetId AND `order` = :order")
    fun getByOrderSync(presetId: Int, order: Int): PresetMapEntity?
}
