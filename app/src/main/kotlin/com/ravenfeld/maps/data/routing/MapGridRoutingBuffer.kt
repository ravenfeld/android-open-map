package com.ravenfeld.maps.data.routing

/**
 * Buffers data while routing for a grid
 *
 *
 * @author Jonas Grunert
 */
data class MapGridRoutingBuffer(val nodeCount: Int) {
    // Buffer for predecessors when calculating routes
    val nodesPreBuffer: LongArray = LongArray(nodeCount)

    // Buffer for node visisted when calculating routes
    val nodesRouteClosedList: BooleanArray = BooleanArray(nodeCount)

    // Buffer for node costs from start when calculating routes
    val nodesRouteCosts: FloatArray = FloatArray(nodeCount)
    val nodesRouteEdges: IntArray = IntArray(nodeCount)
}
