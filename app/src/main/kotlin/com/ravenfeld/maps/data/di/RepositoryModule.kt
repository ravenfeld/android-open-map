package com.ravenfeld.maps.data.di

import com.ravenfeld.maps.data.repository.AltitudeRepositoryImpl
import com.ravenfeld.maps.data.repository.MapRepositoryImpl
import com.ravenfeld.maps.data.repository.PlaceOnMapRepositoryImpl
import com.ravenfeld.maps.data.repository.PresetRepositoryImpl
import com.ravenfeld.maps.data.repository.SearchRepositoryImpl
import com.ravenfeld.maps.data.repository.SettingsRepositoryImpl
import com.ravenfeld.maps.data.repository.TrackRepositoryImpl
import com.ravenfeld.maps.domain.altitude.AltitudeRepository
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.domain.place.repository.PlaceOnMapRepository
import com.ravenfeld.maps.domain.preset.repository.PresetRepository
import com.ravenfeld.maps.domain.search.repository.SearchRepository
import com.ravenfeld.maps.domain.settings.SettingsRepository
import com.ravenfeld.maps.domain.track.TrackRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

/**
 * Object that groups the singletons of repositories.
 */
@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    @ViewModelScoped
    abstract fun bindMapRepository(mapRepositoryImpl: MapRepositoryImpl): MapRepository

    @Binds
    @ViewModelScoped
    abstract fun bindTrackRepository(trackRepositoryImpl: TrackRepositoryImpl): TrackRepository

    @Binds
    @ViewModelScoped
    abstract fun bindSettingsRepository(settingsRepositoryImpl: SettingsRepositoryImpl): SettingsRepository

    @Binds
    @ViewModelScoped
    abstract fun bindAltitudeRepository(altitudeRepositoryImpl: AltitudeRepositoryImpl): AltitudeRepository

    @Binds
    @ViewModelScoped
    abstract fun bindSearchRepository(searchRepositoryImpl: SearchRepositoryImpl): SearchRepository

    @Binds
    @ViewModelScoped
    abstract fun bindPresetRepository(presetRepositoryImpl: PresetRepositoryImpl): PresetRepository

    @Binds
    @ViewModelScoped
    abstract fun bindPoiOnMapRepository(poiOnMapRepository: PlaceOnMapRepositoryImpl): PlaceOnMapRepository
}
