package com.ravenfeld.maps.data.database

import androidx.room.TypeConverter
import kotlinx.serialization.json.Json

class UserActionConverter {
    @TypeConverter
    fun userActionToString(userAction: UserAction?): String? = userAction?.let {
        Json.encodeToString(UserAction.serializer(), userAction)
    }

    @TypeConverter
    fun stringToUserAction(userAction: String): UserAction = Json.decodeFromString(UserAction.serializer(), userAction)
}
