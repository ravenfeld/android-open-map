package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.ElevationsOpenElevationDto
import com.ravenfeld.maps.data.remote.dto.ErrorDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface OpenElevationApi {

    @Headers("Accept: application/json")
    @GET("api/v1/lookup")
    suspend fun elevations(
        @Query("locations") locations: String,
    ): NetworkResponse<ElevationsOpenElevationDto, ErrorDto>
}
