package com.ravenfeld.maps.data.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SearchResponseDto(
    @SerialName("features")
    val results: List<SearchResultDto>,
)

@Serializable
data class SearchResultDto(
    @SerialName("geometry")
    val geometry: Geometry,
    @SerialName("properties")
    val properties: Properties,
)

@Serializable
data class Geometry(
    @SerialName("coordinates")
    val coordinates: List<Double>
)

@Serializable
data class Properties(
    @SerialName("name")
    val name: String? = null,
    @SerialName("country")
    val country: String,
    @SerialName("housenumber")
    val houseNumber: String? = null,
    @SerialName("street")
    val street: String? = null,
    @SerialName("state")
    val state: String? = null,
    @SerialName("city")
    val city: String? = null,
    @SerialName("osm_key")
    val osmKey: String,
    @SerialName("osm_value")
    val osmValue: String,
    @SerialName("osm_id")
    val osmId: Long,
    @SerialName("extent")
    val extent: List<Double>? = null,
    @SerialName("type")
    val type: String? = null,
)
