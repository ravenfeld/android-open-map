package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MapSettingsUpdateEntity(
    @PrimaryKey val name: String,
    val path: String,
    val minZoom: Float,
    val maxZoom: Float
)
