package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Highway
import com.ravenfeld.maps.domain.geo.Surface
import com.ravenfeld.maps.domain.geo.Tag
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.Way
import com.ravenfeld.maps.domain.geo.addWay
import com.ravenfeld.maps.domain.routing.RoutingMode
import java.io.IOException
import java.security.InvalidParameterException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GpxStudioBrouterWebService
@Inject constructor(
    private val gpxStudioBrouterApi: GpxStudioBrouterApi
) {

    @Suppress("MagicNumber")
    suspend fun getTrack(
        departure: GeoPoint,
        arrival: GeoPoint,
        routingMode: RoutingMode
    ): Result<List<TrackPoint>?> =
        when (
            val response = gpxStudioBrouterApi.route(
                "${departure.longitude},${departure.latitude}|${arrival.longitude},${arrival.latitude},",
                when (routingMode) {
                    RoutingMode.HIKE -> "Hiking-Alpine-SAC6"
                    RoutingMode.MOUNTAIN_BIKE -> "MTB"
                    RoutingMode.ROAD_BIKE -> "fastbike"
                    else -> throw InvalidParameterException("routingMode FREE isn't accepted")
                }
            )
        ) {
            is NetworkResponse.Success -> Result.success(
                if (response.body.features.isNotEmpty()) {
                    val features = response.body.features[0]
                    features.geometry.coordinates.mapIndexed { index, coords ->
                        GeoPoint(
                            longitude = coords[0],
                            latitude = coords[1],
                            altitude = if (coords.size >= 3) {
                                coords[2]
                            } else {
                                features.geometry.coordinates.subList(0, index)
                                    .findLast { it.size >= 3 }?.get(2)
                            }
                        )
                    }
                        .addProperties(features.properties.messages)
                } else {
                    null
                }
            )

            is NetworkResponse.Error -> Result.failure(
                response.error ?: IOException("NetworkResponse is on error")
            )
        }

    @Suppress("MagicNumber")
    private fun List<GeoPoint>.addProperties(messages: List<List<String>>): List<TrackPoint> {
        val lngIdx = messages.first().indexOf("Longitude")
        val latIdx = messages.first().indexOf("Latitude")
        val distanceIdx = messages.first().indexOf("Distance")
        val tagsIdx = messages.first().indexOf("WayTags")
        val messagesWithoutHeader = messages.toMutableList()
        messagesWithoutHeader.removeAt(0)

        val ways = mutableListOf<Way>()
        messagesWithoutHeader.forEachIndexed { index, message ->
            if (index == 0) {
                ways.add(
                    Way(
                        start = this.first(),
                        end = GeoPoint(
                            latitude = message[latIdx].toDouble() / 1_000_000,
                            longitude = message[lngIdx].toDouble() / 1_000_000
                        ),
                        startDistance = 0.0,
                        endDistance = message[distanceIdx].toDouble(),
                        tag = message[tagsIdx].toTag()
                    )
                )
            } else {
                val newTag = message[tagsIdx].toTag()
                val lastWay = ways.last()
                if (newTag == lastWay.tag) {
                    ways[ways.lastIndex] = lastWay.copy(
                        end = GeoPoint(
                            latitude = message[latIdx].toDouble() / 1_000_000,
                            longitude = message[lngIdx].toDouble() / 1_000_000
                        ),
                        endDistance = lastWay.endDistance + message[distanceIdx].toDouble(),
                    )
                } else {
                    ways.add(
                        Way(
                            start = lastWay.end,
                            startDistance = lastWay.endDistance,
                            end = GeoPoint(
                                latitude = message[latIdx].toDouble() / 1_000_000,
                                longitude = message[lngIdx].toDouble() / 1_000_000
                            ),
                            endDistance = lastWay.endDistance + message[distanceIdx].toDouble(),
                            tag = message[tagsIdx].toTag()
                        )
                    )
                }
            }
        }
        return addWay(ways)
    }
}

@Suppress("CyclomaticComplexMethod")
private fun String?.toHighway(sacScale: String?): Highway =
    this?.let {
        when {
            it.contains(
                Regex("motorway| motorway_link")
            ) -> Highway.Highway

            it.contains(
                Regex(
                    "motorway|trunk|primary|secondary|tertiary|" +
                        "motorway_link|trunk_link|primary_link|secondary_link|tertiary_link|" +
                        "unclassified|residential"
                )
            ) -> Highway.Road

            it.contains(Regex("living_street|service|pedestrian")) -> Highway.Street
            it.contains(Regex("track")) -> Highway.Track
            it.contains(Regex("cycleway")) -> Highway.Cycleway
            it.contains(Regex("footway")) -> Highway.Footway
            it.contains("path") && (sacScale == "hiking" || sacScale == "strolling") -> Highway.HikingPath
            it.contains("path") && sacScale != null -> Highway.AlpineMountainPath
            it.contains(Regex("path")) -> Highway.Path
            it.contains(Regex("steps")) -> Highway.Steps
            else -> Highway.Unknown
        }
    } ?: Highway.Unknown

@Suppress("CyclomaticComplexMethod")
private fun String?.toSurface(highway: Highway, sacScale: String?): Surface =
    this?.let {
        when {
            it.contains(Regex("asphalt")) -> Surface.Asphalt
            it.contains(Regex("concrete|chipseal|paved")) -> Surface.Tarmac
            it.contains(
                Regex(
                    "sett|unhewn_cobblestone|cobblestone|" +
                        "concrete:lanes|concrete:plates|" +
                        "paving_stones|paving_stones:lanes|" +
                        "grass_paver"
                )
            ) -> Surface.Cobblestone

            it.contains(Regex("unpaved|compacted|fine_gravel")) -> Surface.Compacted
            it.contains(
                Regex(
                    "gravel|shells|pebblestone|" +
                        "ground|dirt|wood|grass|mud|sand|woddchips|rock"
                )
            ) -> Surface.Natural
            sacScale != null -> Surface.Natural
            highway == Highway.Road || highway == Highway.Street -> Surface.Asphalt
            else -> Surface.Unknown
        }
    } ?: run {
        when {
            sacScale != null -> Surface.Natural
            highway == Highway.Road || highway == Highway.Street -> Surface.Asphalt
            else -> Surface.Unknown
        }
    }

private fun String.toTag(): Tag {
    val map = mutableMapOf<String, String>()
    val fields = this.split(" ")
    fields.forEach {
        val (key, value) = it.split("=")
        map[key] = value
    }

    val sacScale = map["sac_scale"]
    val highway = map["highway"].toHighway(sacScale)
    val surface = map["surface"].toSurface(
        highway = highway,
        sacScale = sacScale
    )
    return Tag(
        highway = highway,
        surface = surface
    )
}
