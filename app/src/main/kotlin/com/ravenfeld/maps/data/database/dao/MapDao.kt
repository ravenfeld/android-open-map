package com.ravenfeld.maps.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.ravenfeld.maps.data.database.MapEntity
import com.ravenfeld.maps.data.database.MapPathUpdateEntity
import com.ravenfeld.maps.data.database.MapSettingsUpdateEntity
import kotlinx.coroutines.flow.Flow

@Suppress("TooManyFunctions", "MethodOverloading", "ComplexInterface")
@Dao
interface MapDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(map: MapEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(maps: List<MapEntity>): List<Long>

    @Transaction
    suspend fun insertOrUpdate(maps: List<MapEntity>): List<Long> {
        val insertIds = insert(
            maps.map {
                MapEntity(
                    name = it.name,
                    path = it.path,
                    minZoom = it.minZoom,
                    maxZoom = it.maxZoom
                )
            }
        )
        maps.forEachIndexed { index, map ->
            if (insertIds[index] == -1L) {
                update(
                    MapPathUpdateEntity(
                        name = map.name,
                        path = map.path
                    )
                )
            }
        }
        return insertIds
    }

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(map: MapEntity)

    @Update(entity = MapEntity::class)
    suspend fun update(map: MapSettingsUpdateEntity)

    @Update(entity = MapEntity::class)
    suspend fun update(map: MapPathUpdateEntity)

    @Query("DELETE FROM map WHERE name IN(:names)")
    fun deleteByNames(names: List<String>)

    @Query("DELETE FROM map WHERE name IS :names")
    suspend fun deleteByName(names: String)

    @Query("SELECT COUNT(*) FROM map")
    suspend fun getCountMaps(): Int

    @Query("SELECT * FROM map WHERE path LIKE '%' || :type || '%' ORDER BY name COLLATE NOCASE ASC")
    fun getAllOrderByName(type: String): Flow<List<MapEntity>>

    @Query("SELECT * FROM map ORDER BY name COLLATE NOCASE ASC")
    fun getAllOrderByName(): Flow<List<MapEntity>>

    @Query("SELECT * FROM map WHERE path LIKE '%' || :type || '%' ORDER BY name COLLATE NOCASE ASC")
    fun getAllOrderByNameSync(type: String): List<MapEntity>

    @Query("SELECT * FROM map WHERE name IN(:names)")
    fun getMapByName(names: List<String>): Flow<List<MapEntity>>

    @Query("SELECT * FROM map")
    fun getAllOrderSync(): List<MapEntity>

    @Query("SELECT * FROM map WHERE name IS :name")
    fun get(name: String): Flow<MapEntity>

    @Query("SELECT * FROM map WHERE name IS :name")
    fun getSync(name: String): MapEntity?

    @Query("SELECT * FROM map WHERE groupName IS :name")
    fun getGroupNameSync(name: String): List<MapEntity>
}
