package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.SearchResponseDto
import org.maplibre.android.geometry.LatLng
import timber.log.Timber
import java.io.IOException
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

private val LANGUAGE_SUPPORTED = listOf("en", "fr", "de")

@Singleton
class SearchWebService
@Inject constructor(
    private val searchApi: SearchApi
) {
    suspend fun getSuggestions(query: String, location: LatLng?): Result<SearchResponseDto> =
        when (
            val response = searchApi.search(
                lang = getLocale(),
                query = query,
                lat = location?.latitude,
                lon = location?.longitude
            )
        ) {
            is NetworkResponse.Success -> {
                Result.success(response.body)
            }

            is NetworkResponse.Error -> {
                Timber.e(response.error, "Unwrapping network error")
                Result.failure(response.error ?: IOException())
            }
        }
}

private fun getLocale(): String =
    LANGUAGE_SUPPORTED.find { it == Locale.getDefault().language } ?: "default"
