package com.ravenfeld.maps.data.elevation

import android.content.Context
import com.ravenfeld.maps.domain.geo.GeoPoint
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.ShortBuffer
import java.nio.channels.FileChannel
import java.util.*
import kotlin.math.roundToInt

private const val SECONDS_PER_MINUTE = 60
private const val HGT_EXT = ".hgt"
private const val FOLDER_NAME = "dem"

// alter these values for different SRTM resolutions
private const val HGT_RES = 3 // resolution in arc seconds
private const val HGT_ROW_LENGTH = 1_201 // number of elevation values per line
private const val HGT_VOID = -32_768 // magic number which indicates 'void data' in HGT file

/**
 * Class HgtReader reads data from SRTM HGT files. Currently this class is restricted to a resolution of 3 arc seconds.
 *
 * SRTM data files are available at the [NASA SRTM site](http://dds.cr.usgs.gov/srtm/version2_1/SRTM3)
 * @author Oliver Wieland &lt;oliver.wieland@online.de&gt;
 */
class HgtReader(val context: Context) {
    private val cache = HashMap<String, ShortBuffer?>()
    private var folderHgtExist = false

    init {
        context.getExternalFilesDirs(null).forEach {
            val f = File(it, FOLDER_NAME)
            if (f.exists()) {
                folderHgtExist = true
            }
        }
    }

    fun isReady() = folderHgtExist

    @Suppress("TooGenericExceptionCaught", "NestedBlockDepth")
    fun getGeoPointWithElevationFromHgt(latitude: Double, longitude: Double): GeoPoint {
        return if (folderHgtExist) {
            try {
                val file = getHgtFileName(latitude, longitude)
                // given area in cache?
                if (!cache.containsKey(file)) {
                    // fill initial cache value. If no file is found, then
                    // we use it as a marker to indicate 'file has been searched
                    // but is not there'
                    cache[file] = null
                    context.getExternalFilesDirs(null).forEach {
                        val f = File(it, FOLDER_NAME + File.separator + file)
                        if (f.exists()) {
                            // found something: read HGT file...
                            cache[file] = readHgtFile(f.absolutePath)
                        }
                    }
                }
                // read elevation value
                val altitudeRead = readElevation(latitude, longitude)
                Timber.d("altitude read in file hgt $latitude, $longitude => $altitudeRead")
                GeoPoint(latitude, longitude, altitudeRead)
            } catch (e: FileNotFoundException) {
                Timber.e("Get elevation from HGT $latitude / $longitude  failed: =>  ${e.message}")
                // no problem... file not there
                GeoPoint(latitude, longitude)
            } catch (ioe: Exception) {
                // oops...
                Timber.e("Failed while grid loading:  ${ioe.message}")
                // fallback
                GeoPoint(latitude, longitude)
            }
        } else {
            GeoPoint(latitude, longitude)
        }
    }

    @Throws(Exception::class)
    private fun readHgtFile(file: String): ShortBuffer? {
        var fc: FileChannel? = null
        val sb: ShortBuffer?
        try {
            // Eclipse complains here about resource leak on 'fc' - even with 'finally' clause???
            fc = FileInputStream(file).channel
            // choose the right endianness
            val bb = ByteBuffer.allocateDirect(fc.size().toInt())
            while (bb.remaining() > 0) fc.read(bb)
            bb.flip()
            // sb = bb.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
            sb = bb.order(ByteOrder.BIG_ENDIAN).asShortBuffer()
        } finally {
            fc?.close()
        }
        return sb
    }

    /**
     * Reads the elevation value for the given coordinate.
     *
     * See also [stackexchange.com](http://gis.stackexchange.com/questions/43743/how-to-extract-elevation-from-hgt-file)
     * @param latitude the latitude to get the filename for
     * @param longitude the longitude to get the filename for
     * @return the elevation value or null, if no value is present
     */
    @Suppress("NestedBlockDepth")
    private fun readElevation(latitude: Double, longitude: Double): Double? {
        val tag = getHgtFileName(latitude, longitude)
        val sb = cache[tag]

        if (sb == null) {
            return null
        } else {
            // see http://gis.stackexchange.com/questions/43743/how-to-extract-elevation-from-hgt-file
            val fLat = frac(latitude) * SECONDS_PER_MINUTE
            val fLon = frac(longitude) * SECONDS_PER_MINUTE

            // compute offset within HGT file
            val row = (fLat * SECONDS_PER_MINUTE / HGT_RES).roundToInt()
            val col = (fLon * SECONDS_PER_MINUTE / HGT_RES).roundToInt()
            val list = ArrayList<GeoPoint>()
            for (x in -1..1) {
                for (y in -1..1) {
                    getRoutingPoint(sb, latitude.toLong(), longitude.toLong(), row + x, col + y)
                        .apply {
                            altitude?.let {
                                list.add(this)
                            }
                        }
                }
            }

            return ElevationInterpolator.calculateElevationBasedOnPointList(latitude, longitude, list)
        }
    }

    private fun getRoutingPoint(sb: ShortBuffer, iPartLat: Long, iPartLon: Long, row: Int, col: Int): GeoPoint {
        val latitude = iPartLat + (row * HGT_RES).toDouble() / SECONDS_PER_MINUTE.toDouble() / SECONDS_PER_MINUTE
        val longitude = iPartLon + (col * HGT_RES).toDouble() / SECONDS_PER_MINUTE.toDouble() / SECONDS_PER_MINUTE
        val elevation = readRawElevation(sb, row, col)
        return GeoPoint(latitude, longitude, elevation)
    }

    private fun readRawElevation(sb: ShortBuffer, row: Int, col: Int): Double? {
        val cell = HGT_ROW_LENGTH * (HGT_ROW_LENGTH - row - 1) + col

        // valid position in buffer?
        return if (cell >= 0 && cell < sb.limit()) {
            val ele = sb[cell]
            // check for data voids
            if (ele.toInt() == HGT_VOID) {
                null
            } else {
                ele.toDouble()
            }
        } else {
            null
        }
    }

    /**
     * Gets the associated HGT file name for the given way point. Usually the
     * format is <tt>[N|S]nn[W|E]mmm.hgt</tt> where *nn* is the integral latitude
     * without decimals and *mmm* is the longitude.
     *
     * @param latitude the latitude to get the filename for
     * @param longitude the longitude to get the filename for
     * @return the file name of the HGT file
     */
    private fun getHgtFileName(latitude: Double, longitude: Double): String {
        val lat = latitude.toInt()
        val lon = longitude.toInt()
        var latPref = "N"
        if (lat < 0) latPref = "S"
        var lonPref = "E"
        if (lon < 0) {
            lonPref = "W"
        }
        @Suppress("MagicNumber")
        return "$latPref${lat.format(2)}$lonPref${lon.format(3)}$HGT_EXT"
    }

    private fun Int.format(digits: Int) = "%0${digits}d".format(this)

    private fun frac(d: Double): Double {
        // Get user input
        val iPart: Long = d.toLong()
        return d - iPart
    }
}
