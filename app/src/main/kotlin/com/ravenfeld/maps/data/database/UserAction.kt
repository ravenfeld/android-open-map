package com.ravenfeld.maps.data.database

import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.routing.RoutingMode
import kotlinx.serialization.Serializable

@Serializable
sealed class UserAction {
    @Serializable
    class MoveWaypoint(
        val startPoint: GeoPoint,
        val endPoint: GeoPoint,
        val zoom: Double,
        val routingMode: RoutingMode
    ) : UserAction()

    @Serializable
    class CreateWaypoint(val point: GeoPoint) : UserAction()

    @Serializable
    class CreatePoi(val poi: Poi) : UserAction()
}
