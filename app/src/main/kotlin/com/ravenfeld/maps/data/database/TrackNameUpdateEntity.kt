package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Calendar
import java.util.Date

@Entity
data class TrackNameUpdateEntity(
    @PrimaryKey val id: Int,
    val name: String,
    val updated: Date = Calendar.getInstance().time
)
