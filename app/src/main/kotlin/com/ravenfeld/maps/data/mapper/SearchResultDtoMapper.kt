package com.ravenfeld.maps.data.mapper

import com.ravenfeld.maps.data.remote.dto.Properties
import com.ravenfeld.maps.data.remote.dto.SearchResultDto
import com.ravenfeld.maps.domain.search.model.Suggestion
import com.ravenfeld.maps.domain.search.model.Type
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.geometry.LatLngBounds

fun List<SearchResultDto>.toDomain(): List<Suggestion> = map(SearchResultDto::toDomain)

private fun SearchResultDto.toDomain() =
    Suggestion(
        id = properties.osmId,
        name = properties.name,
        center = LatLng(
            latitude = geometry.coordinates[1],
            longitude = geometry.coordinates[0]
        ),
        bounds = properties.extent?.let {
            @Suppress("MagicNumber")
            LatLngBounds.from(
                it[1],
                it[2],
                it[3],
                it[0],
            )
        },
        city = properties.city,
        houseNumber = properties.houseNumber,
        street = properties.street,
        state = properties.state,
        country = properties.country,
        type = properties.osmValueToType()
    )

@Suppress("CyclomaticComplexMethod")
private fun Properties.osmValueToType() =
    when {
        osmValue == "peak" -> Type.PEAK
        osmValue == "saddle" -> Type.SADDLE
        osmValue == "city" -> Type.CITY
        osmValue == "town" -> Type.CITY
        osmValue == "administrative" && type == "city" -> Type.CITY
        osmValue == "village" -> Type.VILLAGE
        osmValue == "hamlet" -> Type.PLACE
        osmValue == "neighbourhood" -> Type.PLACE
        osmValue == "alpine_hut" -> Type.ALPINE_HUT
        osmValue == "wilderness_hut" -> Type.WILDERNESS_HUT
        osmValue == "cave_entrance" -> Type.CAVE_ENTRANCE
        osmValue == "locality" -> Type.PLACE
        osmValue == "attraction" || osmValue == "museum" -> Type.ATTRACTION
        osmValue == "hotel" -> Type.HOTEL
        osmValue == "water" -> Type.WATER
        osmValue == "spring" -> Type.SPRING
        osmValue == "amenity" -> Type.AMENITY
        osmValue == "viewpoint" -> Type.VIEWPOINT
        osmValue == "place_of_worship" -> Type.PLACE_OF_WORSHIP
        osmValue == "parking" -> Type.PARKING
        osmValue == "train_station" -> Type.TRAIN_STATION
        osmValue == "forest" -> Type.FOREST
        osmValue == "volcano" -> Type.VOLCANO
        osmKey == "building" -> Type.BUILDING
        osmKey == "place" -> Type.PLACE
        else -> Type.OTHER
    }
