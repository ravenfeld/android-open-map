package com.ravenfeld.maps.domain.track

enum class ExportTrackStatus {
    LOADING,
    ERROR_GARMIN_CONNECT,
    ERROR_SERVICE,
    ERROR_TRANSFER,
    ERROR_EMPTY_WATCH,
    ERROR_APP_WATCH_NOT_INSTALLED,
    ERROR_MANY_WATCH,
    WAITING_WATCH_APP,
    SUCCESS,
}
