package com.ravenfeld.maps.domain.track

import android.net.Uri
import com.ravenfeld.maps.domain.event.GetTrackEvent
import com.ravenfeld.maps.domain.event.LoadFileEvent
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.Track
import com.ravenfeld.maps.domain.routing.RoutingMode
import kotlinx.coroutines.flow.Flow

@Suppress("TooManyFunctions", "ComplexInterface")
interface TrackRepository {

    suspend fun loadFile(uri: Uri): Boolean

    fun getLoadFileState(): Flow<LoadFileEvent>

    fun getTracks(): Flow<List<Track>>

    suspend fun remove(trackId: Int)

    suspend fun color(
        trackId: Int,
        color: String
    )

    suspend fun visible(
        trackId: Int,
        visible: Boolean
    )

    suspend fun name(
        trackId: Int,
        name: String
    )

    fun getStateRouting(): Flow<Boolean>

    fun getSegment(
        trackId: Int,
        arrival: GeoPoint,
        backHome: Boolean = false,
        zoom: Double,
        routingMode: RoutingMode
    ): Flow<GetTrackEvent>

    fun moveWaypoint(
        trackId: Int,
        oldWayPoint: GeoPoint,
        newWayPoint: GeoPoint,
        zoom: Double,
        routingMode: RoutingMode
    ): Flow<GetTrackEvent>

    suspend fun addWaypoint(
        trackId: Int,
        waypoint: GeoPoint
    )

    suspend fun startTrack(start: GeoPoint): Int

    suspend fun removeLastPoint(trackId: Int)

    fun cancelLastUserAction(trackId: Int): Flow<GetTrackEvent>

    suspend fun reverse(routeId: Int)

    fun sendTrack(
        name: String,
        trackId: Int
    ): Flow<ExportTrackStatus>

    fun saveGpxFile(
        name: String,
        trackId: Int
    ): Flow<ExportTrackStatus>

    suspend fun savePoi(
        trackId: Int,
        poi: Poi
    )

    suspend fun removePoi(
        trackId: Int,
        point: GeoPoint
    )

    fun resetLoadFile()
}
