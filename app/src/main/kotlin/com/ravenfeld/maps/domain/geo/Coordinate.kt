package com.ravenfeld.maps.domain.geo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Coordinate(
    @SerialName("waypoints")
    val waypoints: MutableList<GeoPoint> = mutableListOf(),
    @SerialName("segments")
    val segments: MutableList<List<TrackPoint>>,
    @SerialName("pois")
    val pois: MutableList<Poi> = mutableListOf(),
)
