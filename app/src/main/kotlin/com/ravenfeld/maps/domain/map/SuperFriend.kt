package com.ravenfeld.maps.domain.map

@Suppress("MaxLineLength")
val mapsSuperFriend = buildList {
    add(
        MapInput(
            name = "IGN Scan 25",
            path = "https://data.geopf.fr/private/wmts?apikey=ign_scan_ws&layer=GEOGRAPHICALGRIDSYSTEMS.MAPS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 16.0f
        )
    )
    add(
        MapInput(
            name = "IGN Pente",
            path = "https://data.geopf.fr/wmts?layer=GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "IGN Trace Hivernale",
            path = "https://data.geopf.fr/wmts?layer=TRACES.RANDO.HIVERNALE&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 16.0f
        )
    )
    add(
        MapInput(
            name = "Sattelite France",
            path = "https://wmts.geopf.fr/wmts?layer=ORTHOIMAGERY.ORTHOPHOTOS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 19.0f
        )
    )
    add(
        MapInput(
            name = "Strava All",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/all/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava Cycling",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/sport_Ride/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava MountainBike",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/sport_MountainBikeRide/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava Gravel",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/sport_GravelRide/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava Run",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/run/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava Backcountry Ski",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/sport_BackcountrySki/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Strava Winter",
            path = "https://proxy.nakarte.me/https/heatmap-external-b.strava.com/tiles-auth/winter/hot/{z}/{x}/{y}.png?px=256",
            minZoom = MIN_ZOOM,
            maxZoom = 15.0f
        )
    )
    add(
        MapInput(
            name = "Swiss Topo",
            path = "https://wmts20.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/3857/{z}/{x}/{y}.jpeg",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "Swiss Topo Pente",
            path = "https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.hangneigung-ueber_30/default/current/3857/{z}/{x}/{y}.png",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "IGN Espagne",
            path = "https://www.ign.es/wmts/mapa-raster?layer=MTN&style=default&tilematrixset=GoogleMapsCompatible&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/jpeg&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 19.0f
        )
    )
    add(
        MapInput(
            name = "Drone restriction",
            path = "https://data.geopf.fr/wmts?layer=TRANSPORTS.DRONES.RESTRICTIONS&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix={z}&TileCol={x}&TileRow={y}",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "OSM Thunderforest Cycle",
            path = "https://a.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=6170aad10dfd42a38d4d8c709a536f38",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "OSM Thunderforest Outdoor",
            path = "https://a.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=6170aad10dfd42a38d4d8c709a536f38",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
    add(
        MapInput(
            name = "Freemap outdoor",
            path = "https://outdoor.tiles.freemap.sk/{z}/{x}/{y}",
            minZoom = MIN_ZOOM,
            maxZoom = 17.0f
        )
    )
}
