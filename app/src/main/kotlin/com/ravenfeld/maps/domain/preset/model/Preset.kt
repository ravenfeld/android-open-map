package com.ravenfeld.maps.domain.preset.model

data class Preset(
    val id: Int,
    val name: String,
    val selected: Boolean
)
