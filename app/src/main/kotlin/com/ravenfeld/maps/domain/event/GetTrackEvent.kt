package com.ravenfeld.maps.domain.event

enum class GetTrackEvent {
    LOADING,
    ERROR,
    SUCCESS
}
