package com.ravenfeld.maps.domain.event

enum class LoadFileEvent {
    LOADING,
    ERROR,
    SUCCESS
}
