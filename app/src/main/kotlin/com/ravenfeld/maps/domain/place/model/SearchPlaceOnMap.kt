package com.ravenfeld.maps.domain.place.model

import org.maplibre.android.geometry.LatLngBounds

data class SearchPlaceOnMap(
    val searchArea: LatLngBounds,
    val typePlaceOnMaps: List<Type>
)
