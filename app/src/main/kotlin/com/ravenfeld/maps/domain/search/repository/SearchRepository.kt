package com.ravenfeld.maps.domain.search.repository

import com.ravenfeld.maps.domain.search.model.Suggestion
import org.maplibre.android.geometry.LatLng

interface SearchRepository {
    suspend fun getSuggestions(query: String, locationUser: LatLng?): List<Suggestion>
}
