package com.ravenfeld.maps.domain.geo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StatsHighway(
    val type: Highway,
    val distance: Double,
    val percent: Int
) : Parcelable
